# [<< Final Exam](./README.md) < Question 6 - Answer

**What could potentially improve the speed of inserts?**
Check all that apply.

 1. Add an index on last_name, first_name if one does not already exist.
 1. **Remove all indexes from the collection**
 1. Provide a hint to MongoDB that it should not use an index for the inserts
 1. **Set w=0, j=0 on writes**
 1. Build a replica set and insert data into the secondary nodes to free up the primary nodes.

_Answers are: **2 and 4**_

See [stackoverflow](http://stackoverflow.com/questions/16011193/improve-the-speed-of-inserts-in-mongodb)