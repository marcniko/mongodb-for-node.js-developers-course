use enron
db.messages.aggregate([
  {$project:
    {
      _id: 1,
      'from': "$headers.From",
      'to' : "$headers.To",
    }
  },
  {$unwind: "$to"},
  {$group:
    {
      _id: {_id: "$_id", from: "$from"},
      to: {$addToSet: "$to"}
    }
  },
  {$unwind: "$to"},
  {$group:
    {
      _id: {from: "$_id.from", to: "$to"},
      count: {$sum: 1}
    }
  },
  {$sort: {count: -1}},
  {$limit: 5}
])
