# [<< Final Exam](./README.md) < Question 8 - Answer

**Will there be a rollback of data on Node 1 when Node 1 comes back up?**
Choose the best answer.

 * Yes, always
 * No, never
 * Maybe, it depends on whether Node 3 has processed the write
 * **Maybe, it depends on whether Node 2 has processed the write**

_Answer is: **Maybe, it depends on whether Node 2 has processed the write**_

See similar question, different delay (10 seconds) at [stackoverflow](http://stackoverflow.com/questions/15998819/mongodb-roll-back-in-replica-set)