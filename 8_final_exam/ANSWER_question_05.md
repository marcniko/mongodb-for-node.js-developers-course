# [<< Final Exam](./README.md) < Question 5 - Answer

Recreate the situation: collection _fubar_ inside database _test_, creating indexes:
```
use test
db.createCollection('fubar')
db.fubar.ensureIndex({a:1, b:1})
db.fubar.ensureIndex({a:1, c:1})
db.fubar.ensureIndex({c:1})
db.fubar.ensureIndex({a:1, b:1, c:-1})
db.fubar.getIndexes()
```
Query explain:
```
db.fubar.find({'a':{'$lt':10000}, 'b':{'$gt': 5000}}, {'a':1, 'c':1}).sort({'c':-1}).explain()
{
	"cursor" : "BtreeCursor a_1_b_1",
...
```

Dropping index used and explaining again:
```
db.fubar.dropIndex({a:1, b:1})
db.fubar.find({'a':{'$lt':10000}, 'b':{'$gt': 5000}}, {'a':1, 'c':1}).sort({'c':-1}).explain()
{
	"cursor" : "BtreeCursor a_1_c_1",
...

db.fubar.dropIndex({a:1, c:1})
db.fubar.find({'a':{'$lt':10000}, 'b':{'$gt': 5000}}, {'a':1, 'c':1}).sort({'c':-1}).explain()
{
	"cursor" : "BtreeCursor c_1 reverse",
...

db.fubar.dropIndex({c:1})
db.fubar.find({'a':{'$lt':10000}, 'b':{'$gt': 5000}}, {'a':1, 'c':1}).sort({'c':-1}).explain()
{
	"cursor" : "BtreeCursor a_1_b_1_c_-1",
...

db.fubar.dropIndex({a:1, b:1, c:-1})
db.fubar.find({'a':{'$lt':10000}, 'b':{'$gt': 5000}}, {'a':1, 'c':1}).sort({'c':-1}).explain()
{
	"cursor" : "BasicCursor",
...
```


**Which of the following indexes could be used by MongoDB to assist in answering the query.**
Check all that apply.

 * _id_
 * **a_1_b_1**
 * **a_1_c_1**
 * **c_1**
 * **a_1_b_1_c_-1**

_Answers are: **all except _id**_