# Final Exam

## Question 1
_[Question 1](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Final_Exam/52b87c78e2d42307189eef2b/)_

Please download the Enron email dataset [enron.zip](./enron.zip) (from S3: [enron.zip](https://s3.amazonaws.com/edu-downloads.10gen.com/enron/enron.zip)), unzip it and then restore it using mongorestore. It should restore to a collection called "messages" in a database called "enron". Note that this is an abbreviated version of the full corpus. There should be 120,477 documents after restore. 

``` 
unzip enron.zip
mongorestore dump
``` 

Inspect a few of the documents to get a basic understanding of the structure. Enron was an American corporation that engaged in a widespread accounting fraud and subsequently failed. 

In this dataset, each document is an email message. Like all Email messages, there is one sender but there can be multiple recipients. 

Construct a query to calculate the number of messages sent by Andrew Fastow, CFO, to Jeff Skilling, the president. Andrew Fastow's email addess was andrew.fastow@enron.com. Jeff Skilling's email was jeff.skilling@enron.com. 

For reference, the number of email messages from Andrew Fastow to John Lavorato (john.lavorato@enron.com) was 1. 

 * 1
 * 3
 * 5
 * 7
 * 9
 * 12

See [answer](./ANSWER_question_01.md)





## Question 2
_[Question 2](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Final_Exam/52b88246e2d42307189eef2e/)_

Please use the Enron dataset you imported for the previous problem. For this question you will use the aggregation framework to figure out pairs of people that tend to communicate a lot. To do this, you will need to unwind the To list for each message. 

This problem is a little tricky because a recipient may appear more than once in the To list for a message. You will need to fix that in a stage of the aggregation before doing your grouping and counting of (sender, recipient) pairs. 

Which pair of people have the greatest number of messages in the dataset?

 * susan.mara@enron.com to jeff.dasovich@enron.com
 * susan.mara@enron.com to richard.shapiro@enron.com
 * soblander@carrfut.com to soblander@carrfut.com
 * susan.mara@enron.com to james.steffes@enron.com
 * evelyn.metoyer@enron.com to kate.symes@enron.com
 * susan.mara@enron.com to alan.comnes@enron.com

See [answer](./ANSWER_question_02.md)






## Question 3
_[Question 3](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Final_Exam/52e001c6e2d423744501d039/)_

In this problem you will update a document in the Enron dataset to illustrate your mastery of updating documents from the shell. 

Please add the email address "mrpotatohead@mongodb.com" to the list of addresses in the "headers.To" array for the document with "headers.Message-ID" of "<8147308.1075851042335.JavaMail.evans@thyme>" 

After you have completed that task, please download [final3.zip](./final3.zip) (from MongoDB: [final3.zip](https://education.mongodb.com/static/10gen_2014_M101JS_January/handouts/final3.77df7470d534.zip)) from the Download Handout link and run final3-validate.js to get the validation code and put it in the box below without any extra spaces. The validation script assumes that it is connecting to a simple mongo instance on the standard port on localhost.

``` 
unzip final3.zip
cd Final3
npm install
node final3-validate.js
``` 

See [answer](./ANSWER_question_03.md)




## Question 4
_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Final_Exam/52dffeece2d423744501d029/)_
_[@Youtube list](https://www.youtube.com/watch?v=E7-wlLWkP00&list=PLUVUdDGxXsziwqStzVykOrfpSBg6RzHaq)_

### Enhancing the Blog to support viewers liking certain comments
In this problem, you will be enhancing the blog project to support users liking certain comments and the like counts showing up the in the permalink page. 

Start by downloading [final4.zip](./final4.zip) (from MongoDB: [final4.zip](https://education.mongodb.com/static/10gen_2014_M101JS_January/handouts/final4.c84bf2147329.zip)) and [posts.json](./posts.json) (from MongoDB: [posts.json](https://education.mongodb.com/static/10gen_2014_M101JS_January/handouts/posts.f52bca51f2fb.json)) from the Download Handout link and loading up the blog dataset posts.json. The user interface has already been implemented for you. It's not fancy. The /post URL shows the like counts next to each comment and displays a Like button that you can click on. That Like button POSTS to the /like URL on the blog, makes the necessary changes to the database state (you are implementing this), and then redirects the browser back to the permalink page. 

This full round trip and redisplay of the entire web page is not how you would implement liking in a modern web app, but it makes it easier for us to reason about, so we will go with it. 

Your job is to search the code for the string "XXX work here" and make any necessary changes. You can choose whatever schema you want, but you should note that the entry_template makes some assumptions about the how the like value will be encoded and if you go with a different convention than it assumes, you will need to make some adjustments. 

The validation script does not look at the database. It looks at the blog. 

The validation script, final4-validate.js, will fetch your blog, go to the first post's permalink page and attempt to increment the vote count. You run it as follows:

``` 
node final4-validate.js
``` 

Remember that the blog needs to be running as well as Mongo. The validation script takes some options if you want to run outside of localhost. 

``` 
unzip final4.zip
cd final4/validate/
npm install
``` 

See [answer](./ANSWER_question_04.md)









## Question 5
_[Question 5](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Final_Exam/52b891d7e2d42307189eef3a/)_

Suppose your have a collection _fubar_ with the following indexes created:

```
[
	{
		"v" : 1,
		"key" : {
			"_id" : 1
		},
		"ns" : "test.fubar",
		"name" : "_id_"
	},
	{
		"v" : 1,
		"key" : {
			"a" : 1,
			"b" : 1
		},
		"ns" : "test.fubar",
		"name" : "a_1_b_1"
	},
	{
		"v" : 1,
		"key" : {
			"a" : 1,
			"c" : 1
		},
		"ns" : "test.fubar",
		"name" : "a_1_c_1"
	},
	{
		"v" : 1,
		"key" : {
			"c" : 1
		},
		"ns" : "test.fubar",
		"name" : "c_1"
	},
	{
		"v" : 1,
		"key" : {
			"a" : 1,
			"b" : 1,
			"c" : -1
		},
		"ns" : "test.fubar",
		"name" : "a_1_b_1_c_-1"
	}
]
```

Now suppose you want to run the following query against the collection.

```
db.fubar.find({'a':{'$lt':10000}, 'b':{'$gt': 5000}}, {'a':1, 'c':1}).sort({'c':-1})
```

Which of the following indexes could be used by MongoDB to assist in answering the query.
Check all that apply.

 * _id_
 * a_1_b_1
 * a_1_c_1
 * c_1
 * a_1_b_1_c_-1

See [answer](./ANSWER_question_05.md)





## Question 6
_[Question 6](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Final_Exam/52b8924ce2d42307189eef3d/)_

Suppose you have a collection of _students_ of the following form:

```
{
	"_id" : ObjectId("50c598f582094fb5f92efb96"),
	"first_name" : "John",
	"last_name" : "Doe",
	"date_of_admission" : ISODate("2010-02-21T05:00:00Z"),
	"residence_hall" : "Fairweather",
	"has_car" : true,
	"student_id" : "2348023902",
	"current_classes" : [
		"His343",
		"Math234",
		"Phy123",
		"Art232"
	]
}
```

Now suppose that basic inserts into the collection, which only include the last name, first name and student_id, are too slow (we can't do enough of them per second from our program). What could potentially improve the speed of inserts. Check all that apply.

 * Add an index on last_name, first_name if one does not already exist.
 * Remove all indexes from the collection
 * Provide a hint to MongoDB that it should not use an index for the inserts
 * Set w=0, j=0 on writes
 * Build a replica set and insert data into the secondary nodes to free up the primary nodes.

See [answer](./ANSWER_question_06.md)






## Question 7
_[Question 7](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Final_Exam/52e000bce2d423744501d030/)_

You have been tasked to cleanup a photosharing database. The database consists of two collections, albums, and images. Every image is supposed to be in an album, but there are orphan images that appear in no album. Here are some example documents (not from the collections you will be downloading). 

```
> db.albums.findOne()
{
	"_id" : 67
	"images" : [
		4745,
		7651,
		15247,
		17517,
		17853,
		20529,
		22640,
		27299,
		27997,
		32930,
		35591,
		48969,
		52901,
		57320,
		96342,
		99705
	]
}

> db.images.findOne()
{ "_id" : 99705, "height" : 480, "width" : 640, "tags" : [ "dogs", "kittens", "work" ] }

```

From the above, you can conclude that the image with _id = 99705 is in album 67. It is not an orphan. 

Your task is to write a program to remove every image from the images collection that appears in no album. Or put another way, if an image does not appear in at least one album, it's an orphan and should be removed from the images collection. 

Download and unzip [final7.zip](./final7.zip) (from MongoDB: [final7.zip](https://education.mongodb.com/static/10gen_2014_M101JS_January/handouts/final7.55e3678c7664.zip)) and use mongoimport to import the collections in albums.json and images.json. 

```
mongoimport -d photosharing - c albums < albums.json
mongoimport -d photosharing - c images < images.json
```

When you are done removing the orphan images from the collection, there should be 89,737 documents in the images collection.
To prove you did it correctly, **what are the total number of images with the tag 'kittens" after the removal of orphans?**
As as a sanity check, there are 49,932 images that are tagged 'kittens' before you remove the images. 
_Hint: you might consider creating an index or two or your program will take a long time to run._

 * 49,932
 * 47,678
 * 38,934
 * 45,911
 * 44,822

See [answer](./ANSWER_question_07.md)






## Question 8
_[Question 8](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Final_Exam/52e03176e2d423744501d055/)_

Suppose you have a three node replica set. Node 1 is the primary. Node 2 is a secondary, Node 3 is a secondary running with a delay of two hours. All writes to the database are issued with _w=majority_ and _j=1_ (by which we mean that the getLastError call has those values set). 

A write operation (could be insert or update) is initiated from your application using the Node.js driver at time=0. At time=5 seconds, the primary, Node 1, goes down for an hour and node 2 is elected primary. Note that your write operation has not yet returned at the time of the failure. Note also that although you have not received a response from the write, it has been processed and written by Node 1 before the failure. Node 3, since it has a slave delay option set, is lagging. 

Will there be a rollback of data on Node 1 when Node 1 comes back up? Choose the best answer.

 * Yes, always
 * No, never
 * Maybe, it depends on whether Node 3 has processed the write
 * Maybe, it depends on whether Node 2 has processed the write

See [answer](./ANSWER_question_08.md)





## Question 9
_[Question 9](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Final_Exam/52b893fde2d42307189eef46/)_

Imagine an electronic medical record database designed to hold the medical records of every individual in the United States. Because each person has more than 16MB of medical history and records, it's not feasible to have a single document for every patient. Instead, there is a patient collection that contains basic information on each person and maps the person to a patient_id, and a _record_ collection that contains one document for each test or procedure. One patient may have dozens or even hundreds of documents in the _record_ collection. 

We need to decide on a shard key to shard the _record_ collection. What's the best shard key for the _record_ collection, provided that we are willing to run inefficient scatter-gather operations to do infrequent research and run studies on various diseases and cohorts? That is, think mostly about the operational aspects of such a system. And by operational, we mean, think about what the most common operations that this systems needs to perform day in and day out.

 * patient_id
 * _id
 * Primary care physician (your principal doctor that handles everyday problems)
 * Date and time when medical record was created
 * Patient first name
 * Patient last name

See [answer](./ANSWER_question_09.md)







## Question 10
_[Question 10](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Final_Exam/52b89464e2d42307189eef49/)_

**Understanding the output of explain** We perform the following query on the enron dataset:

```
db.messages.find({'headers.Date':{'$gt': new Date(2001,3,1)}},{'headers.From':1, _id:0}).sort({'headers.From':1}).explain()
```

and get the following explain output.

```
{
	"cursor" : "BtreeCursor headers.From_1",
	"isMultiKey" : false,
	"n" : 83057,
	"nscannedObjects" : 120477,
	"nscanned" : 120477,
	"nscannedObjectsAllPlans" : 120581,
	"nscannedAllPlans" : 120581,
	"scanAndOrder" : false,
	"indexOnly" : false,
	"nYields" : 0,
	"nChunkSkips" : 0,
	"millis" : 250,
	"indexBounds" : {
		"headers.From" : [
			[
				{
					"$minElement" : 1
				},
				{
					"$maxElement" : 1
				}
			]
		]
	},
	"server" : "Andrews-iMac.local:27017"
}
```

Check below all the statements that are true about the way MongoDB handled this query.

 1. The query did not utilize an index to figure out which documents match the find criteria.
 1. The query used an index for the sorting phase.
 1. The query returned 120,477 documents
 1. The query performed a full collection scan

See [answer](./ANSWER_question_10.md)



See others final exams: [http://techinote.com/mongodb-final-exam/](http://techinote.com/mongodb-final-exam/)
