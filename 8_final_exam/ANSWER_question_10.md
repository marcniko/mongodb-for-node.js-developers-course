# [<< Final Exam](./README.md) < Question 10 - Answer

Check below all the statements that are true about the way MongoDB handled this query.

 1. **The query did not utilize an index to figure out which documents match the find criteria.**
 1. **The query used an index for the sorting phase.**
 1. The query returned 120,477 documents
 1. **The query performed a full collection scan**

_Answers are: **1, 2 and 4**_