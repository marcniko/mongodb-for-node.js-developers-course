# [<< Final Exam](./README.md) < Question 9 - Answer

**What's the best shard key for the _record_ collection, provided that we are willing to run inefficient scatter-gather operations to do infrequent research and run studies on various diseases and cohorts?**
That is, think mostly about the operational aspects of such a system. And by operational, we mean, think about what the most common operations that this systems needs to perform day in and day out.

 * **patient_id**
 * _id
 * Primary care physician (your principal doctor that handles everyday problems)
 * Date and time when medical record was created
 * Patient first name
 * Patient last name

_Answer is: **patient_id**_