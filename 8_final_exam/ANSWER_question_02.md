# [<< Final Exam](./README.md) < Question 2 - Answer

**Which pair of people have the greatest number of messages in the dataset?**

 * **susan.mara@enron.com to jeff.dasovich@enron.com**
 * susan.mara@enron.com to richard.shapiro@enron.com
 * soblander@carrfut.com to soblander@carrfut.com
 * susan.mara@enron.com to james.steffes@enron.com
 * evelyn.metoyer@enron.com to kate.symes@enron.com
 * susan.mara@enron.com to alan.comnes@enron.com

_Answer is: **susan.mara@enron.com to jeff.dasovich@enron.com**_

First solution:

```
mongo < query_02_solution_01.js
```

```
> db.messages.aggregate([
  {$project:
    {
      _id: 1,
      'from': "$headers.From",
      'to' : "$headers.To",
    }
  },
  {$unwind: "$to"},
  {$group:
    {
      _id: {_id: "$_id", from: "$from", to: "$to"}
    }
  },
  {$group:
    {
      _id: {from: "$_id.from", to: "$_id.to"},
      count: {$sum: 1}
    }
  },
  {$sort: {count: -1}},
  {$limit: 5}
])
```

Second solution:

```
mongo < query_02_solution_02.js
```

```
> db.messages.aggregate([
  {$project:
    {
      _id: 1,
      'from': "$headers.From",
      'to' : "$headers.To",
    }
  },
  {$unwind: "$to"},
  {$group:
    {
      _id: {_id: "$_id", from: "$from"},
      to: {$addToSet: "$to"}
    }
  },
  {$unwind: "$to"},
  {$group:
    {
      _id: {from: "$_id.from", to: "$to"},
      count: {$sum: 1}
    }
  },
  {$sort: {count: -1}},
  {$limit: 5}
])
```

Output:
```
{
  "result" : [
    {
      "_id" : {
        "from" : "susan.mara@enron.com",
        "to" : "jeff.dasovich@enron.com"
      },
      "count" : 750
    },
    {
      "_id" : {
        "from" : "soblander@carrfut.com",
        "to" : "soblander@carrfut.com"
      },
      "count" : 679
    },
    {
      "_id" : {
        "from" : "susan.mara@enron.com",
        "to" : "james.steffes@enron.com"
      },
      "count" : 646
    },
    {
      "_id" : {
        "from" : "susan.mara@enron.com",
        "to" : "richard.shapiro@enron.com"
      },
      "count" : 616
    },
    {
      "_id" : {
        "from" : "evelyn.metoyer@enron.com",
        "to" : "kate.symes@enron.com"
      },
      "count" : 567
    }
  ],
  "ok" : 1
}
```
