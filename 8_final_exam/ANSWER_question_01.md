# [<< Final Exam](./README.md) < Question 1 - Answer

**Construct a query to calculate the number of messages sent by Andrew Fastow, CFO, to Jeff Skilling, the president. Andrew Fastow's email addess was andrew.fastow@enron.com. Jeff Skilling's email was jeff.skilling@enron.com.**

For reference, the number of email messages from Andrew Fastow to John Lavorato (john.lavorato@enron.com) was 1. 

 * 1
 * **3**
 * 5
 * 7
 * 9
 * 12

_Answer is: **3**_

```
> db.messages.count({ 'headers.From': 'andrew.fastow@enron.com', 'headers.To': 'jeff.skilling@enron.com' })
3
```

#Reference from Andrew Fastow to John Lavorato (john.lavorato@enron.com) was 1:

```
> db.messages.count({ 'headers.From': 'andrew.fastow@enron.com', 'headers.To': 'john.lavorato@enron.com' })
1
```