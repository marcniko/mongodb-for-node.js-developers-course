# [<< Final Exam](./README.md) < Question 7 - Answer

**What are the total number of images with the tag 'kittens" after the removal of orphans?**

As as a sanity check, there are 49,932 images that are tagged 'kittens' before you remove the images. 

```
db.images.count({tags: 'kittens'})
49932
```

_Hint: you might consider creating an index or two or your program will take a long time to run._

See [task_07.js](./task_07.js)

```
mongo < task_07.js
```

```
use photosharing
db.albums.ensureIndex({images : 1});

cur = db.images.find({}); null;
while(cur.hasNext()){
    img = cur.next(); null;
    if (db.albums.count({images : img._id}) == 0)
       db.images.remove({_id: img._id});
}

db.images.count({tags: 'kittens'});
```

 * 49,932
 * 47,678
 * 38,934
 * 45,911
 * **44,822**

_Answer is: **44822**_