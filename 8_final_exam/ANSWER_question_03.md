# [<< Final Exam](./README.md) < Question 3 - Answer

Please add the email address "mrpotatohead@mongodb.com" to the list of addresses in the "headers.To" array for the document with "headers.Message-ID" of "<8147308.1075851042335.JavaMail.evans@thyme>" 

Find:
```
db.messages.find({"headers.Message-ID": "<8147308.1075851042335.JavaMail.evans@thyme>"}).pretty()
```

Update:
```
db.messages.update(
  {"headers.Message-ID": "<8147308.1075851042335.JavaMail.evans@thyme>"},
  { $addToSet : { "headers.To" : "mrpotatohead@mongodb.com" } }
);
``` 

``` 
> node final3-validate.js
Welcome to the Final Exam Q3 Checker. My job is to make sure you correctly updated the document
Final Exam Q3 Validated successfully!
Your validation code is: vOnRg05kwcqyEFSve96R
``` 

_Answer is: **vOnRg05kwcqyEFSve96R**_