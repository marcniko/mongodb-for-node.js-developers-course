# [<< Final Exam](./README.md) < Question 4 - Answer

Implementing code in: **posts.js**, modify the function **incrementLikes()** used by function _handleLike()_ at _content.js_:

```
    this.incrementLikes = function(permalink, comment_ordinal, callback) {
        "use strict";

        /*
         * Example selector
        var selector_example = {};
        var comment_ordinal_example = 0;
        selector_example['comments.' + comment_ordinal_example + '.author'] = 'Frank';
        */

        // TODO: Final exam question - Increment the number of likes
        // callback(Error("incrementLikes NYI"), null);
        var updateNumLikes = {};
        updateNumLikes['comments.' + comment_ordinal + '.num_likes'] = 1;
        posts.update({'permalink': permalink}, {$inc: updateNumLikes}, function(err, post) {
            "use strict";

            if (err) return callback(err, null);

            callback(err, post);
        });
    }

``` 

Validating:
``` 
> node final4-validate.js
Trying to fetch blog homepage for url http://localhost:3000/
Trying to grab the number of likes for url http://localhost:3000/post/gqmhLUMMDrdApkNyopbh
Trying to increment the number of likes for post: /post/gqmhLUMMDrdApkNyopbh
Trying to grab the number of likes for url http://localhost:3000/post/gqmhLUMMDrdApkNyopbh
Successfully clicked like
Blog validated successfully!
Your validation code is: VQ3jedFjG5VmElLTYKqS
``` 

_Answer is: **VQ3jedFjG5VmElLTYKqS**_