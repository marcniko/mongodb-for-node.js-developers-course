# [<< Week 6](./README.md) < 18. Choosing a Shard Key - Answer
_[@Youtube video answer](https://www.youtube.com/watch?v=91tO_iGga5w&list=PLUVUdDGxXszgq6abyzMsbQ4omDp6UL2J1)_

**Thinking about the tradeoffs of shard key selection, select the true statements below.**

 1. **Choosing posttime as the shard key will cause hotspotting as time progresses.**
 1. **Choosing username as the shard key will distribute posts to the wall well across the shards.**
 1. **Choosing visible_to as a shard key is illegal.**
 1. Choosing posttime as the shard key suffers from low cardinality.

_Answers are: **1, 2 and 3**_
