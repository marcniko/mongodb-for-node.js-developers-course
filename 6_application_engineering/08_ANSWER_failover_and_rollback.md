# [<< Week 6](./README.md) < 8. Failover and Rollback - Answer
_[@Youtube video answer](https://www.youtube.com/watch?v=t3rNxRg6WZE&list=PLUVUdDGxXszgq6abyzMsbQ4omDp6UL2J1)_

**What happens if a node comes back up as a secondary after a period of being offline and the oplog has looped on the primary?**

 * **The entire dataset will be copied from the primary**
 * A rollback will occur
 * The new node stays offline (does not re-join the replica set)
 * The new node begins to calculate Pi to a large number of decimal places

_Answer is: **The entire dataset will be copied from the primary**_