# [<< Week 6](./README.md) < 3. Replica Set Elections - Answer
_[@Youtube video answer](https://www.youtube.com/watch?v=vrUxTTD-XwA&list=PLUVUdDGxXszgq6abyzMsbQ4omDp6UL2J1)_

**Which types of nodes can participate in elections of a new primary?**

 * **Regular replica set members**
 * **Hidden Members**
 * **Arbiters**
 * Lawyers

_Answers are: **all except Lawyers**_
