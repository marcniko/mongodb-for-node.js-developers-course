# [<< Week 6](../README.md) < 10. Failover in the Node.js Driver - Answer
_[@Youtube video answer](https://www.youtube.com/watch?v=tnf5m6M9-Bo&list=PLUVUdDGxXszgq6abyzMsbQ4omDp6UL2J1)_

```
db.collection('foo').insert({x:1}, callback);
```

**What will happen if this insert happens during a primary election?**

 * The insert will immediately succeed and the callback will be called
 * The insert will fail with an error
 * **The insert will be buffered until the election completes, then the callback will be called after the operation is sent and a response is received**
 * The callback will be called first, then the insert will be buffered until the election completes

_Answer is: **The insert will be buffered until the election completes, then the callback will be called after the operation is sent and a response is received**_