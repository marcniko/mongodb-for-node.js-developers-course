# [<< Week 6](./README.md) < 4. Write Consistency - Answer
_[@Youtube video answer](https://www.youtube.com/watch?v=SjaJEYK1u3E&list=PLUVUdDGxXszgq6abyzMsbQ4omDp6UL2J1)_

**During the time when failover is occurring, can writes successfully complete?**

 * Yes
 * **No**

_Answer is: **No**_
