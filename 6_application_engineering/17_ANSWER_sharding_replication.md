# [<< Week 6](./README.md) < 17. Sharding + Replication - Answer
_[@Youtube video answer](https://www.youtube.com/watch?v=hnuiYokdO7g&list=PLUVUdDGxXszgq6abyzMsbQ4omDp6UL2J1)_

**Suppose you want to run multiple mongos routers for redundancy. What level of the stack will assure that you can failover to a different mongos from within your application?**

 * mongod
 * mongos
 * **drivers**
 * sharding config servers

_Answer is: **drivers**_
