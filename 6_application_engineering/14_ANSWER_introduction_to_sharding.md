# [<< Week 6](./README.md) < 14. Introduction to Sharding - Answer
_[@Youtube video answer](http://www.youtube.com/watch?v=4Z0D2bX5Cg4&list=PLUVUdDGxXszgq6abyzMsbQ4omDp6UL2J1)_

**If the shard key is not include in a find operation and there are 3 shards, each one a replica set with 3 nodes, how many nodes will see the find operation?**

 * 1
 * **3**
 * 9
 * 6

_Answer is: **3**_
