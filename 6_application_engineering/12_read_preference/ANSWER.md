# [<< Week 6](../README.md) < 12. Read Preferences - Answer
_[@Youtube video answer](http://www.youtube.com/watch?v=FheFRpktCcI&list=PLUVUdDGxXszgq6abyzMsbQ4omDp6UL2J1)_

**You can configure your applications via the drivers to read from secondary nodes within a replica set. What are the reasons that you might not want to do that? Check all that apply.**

 * **If your write traffic is significantly greater than your read traffic, you may overwhelm the secondary, which must process all the writes as well as the reads. Replication lag can result.**
 * **You may not read what you previously wrote to MongoDB.**
 * **If the secondary hardware has insufficient memory to keep the read working set in memory, directing reads to it will likely slow it down.**
 * Reading from a secondary prevents it from being promoted to primary.

_Answers are: **all except last option**_
