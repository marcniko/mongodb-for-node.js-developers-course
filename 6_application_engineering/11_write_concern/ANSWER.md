# [<< Week 6](../README.md) < 11. Write Concern - Answer
_[@Youtube video answer](https://www.youtube.com/watch?v=HqtfMarARBQ&list=PLUVUdDGxXszgq6abyzMsbQ4omDp6UL2J1)_

**What happens if we specify a write concern larger than the number of nodes we currently have up?**

 * **The write waits forever.**
 * The driver throws an error.
 * The server throws an error.
 * The write goes to as many nodes as possible then the driver returns success.

_Answer is: **The write waits forever**_
