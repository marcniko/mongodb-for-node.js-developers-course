# [<< Week 6](../README.md) < 5. Creating a Replica Set - Answer
_[@Youtube video answer](http://www.youtube.com/watch?v=ibZVf59aUj8&list=PLUVUdDGxXszgq6abyzMsbQ4omDp6UL2J1)_

**Which command, when issued from the mongo shell, will allow you to read from a secondary?**

 * db.isMaster()
 * db.adminCommand({'readPreference':'Secondary'})
 * rs.setStatus("Primary")
 * **rs.slaveOk()**

_Answer is: **rs.slaveOk()**_
