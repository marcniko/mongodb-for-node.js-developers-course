# [<< Week 6](./README.md) < 7. Replica Set Internals - Answer
_[@Youtube video answer](http://www.youtube.com/watch?v=_am2mdVF1uk&list=PLUVUdDGxXszgq6abyzMsbQ4omDp6UL2J1)_

**In the video how long did it take to elect a new primary?**

 * **About three seconds**
 * Above 10 seconds
 * About 30 seconds
 * About a minute

_Answer is: **About three seconds**_
