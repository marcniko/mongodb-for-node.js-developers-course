# [<< Week 6](../README.md) < 15. Building a Sharded Environment - Answer
_[@Youtube video answer](http://www.youtube.com/watch?v=5tPjnJrY0rU&list=PLUVUdDGxXszgq6abyzMsbQ4omDp6UL2J1)_

**If you want to build a production system with two shards, each one a replica set with three nodes, how may mongod processes must you start?**

 * 2
 * 6
 * 7
 * **9**


_Answer is: **9**_
