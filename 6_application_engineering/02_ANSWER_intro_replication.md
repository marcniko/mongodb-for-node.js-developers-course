# [<< Week 6](./README.md) < 2. Introduction to Replication - Answer
_[@Youtube video answer](https://www.youtube.com/watch?v=XCssKzhlnZU&list=PLUVUdDGxXszgq6abyzMsbQ4omDp6UL2J1)_

**What is the minimum original number of nodes needed to assure the election of a new Primary if a node goes down?**

 * 1
 * 2
 * **3**
 * 5

_Answer is: **3**_
