# [<< Week 6](./README.md) < 16. implications of Sharding - Answer
_[@Youtube video answer](https://www.youtube.com/watch?v=vfl0hqdgB3Q&list=PLUVUdDGxXszgq6abyzMsbQ4omDp6UL2J1)_

**Suppose you wanted to shard the zip code collection after importing it. You want to shard on zip code. What index would be required to allow MongoDB to shard on zip code?**

 * **An index on zip or a non-multi-key index that starts with zip.**
 * No index is required to use zip as the shard key.
 * A unique index on the zip code.
 * Any index that that includes the zip code.

_Answer is: **An index on zip or a non-multi-key index that starts with zip.**_
