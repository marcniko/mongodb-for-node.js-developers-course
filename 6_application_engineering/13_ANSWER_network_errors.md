# [<< Week 6](./README.md) < 13. Network Errors - Answer
_[@Youtube video answer](http://www.youtube.com/watch?v=64mAA3SEnkg&list=PLUVUdDGxXszgq6abyzMsbQ4omDp6UL2J1)_

**What are the reasons why an application may receive an error back even if the write was successful. Check all that apply.**

 * **The network TCP network connection between the application and the server was reset between the time of the write and the time of the getLastError call.**
 * **The MongoDB server terminates between the write and the getLastError call.**
 * **The network fails between the time of the write and the time of the getLastError call**
 * The write violates a primary key constraint on the collection and must be rolled back.

_Answers are: **all except last one**_
