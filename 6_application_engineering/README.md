# Week 6 Appplication Engineering
_[List videos @Youtube](https://www.youtube.com/playlist?list=PLUVUdDGxXszgq6abyzMsbQ4omDp6UL2J1)_

## 1. Introduction to Week 6
_[Video introduction](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_6_Application_Engineering/52b3454fe2d42362670d826c/) - [@Youtube list](https://www.youtube.com/watch?v=3zcbhNHebAc&list=PLUVUdDGxXszgq6abyzMsbQ4omDp6UL2J1)_





## 2. Introduction to Replication
_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_6_Application_Engineering/52b359c2e2d423678d3b9d38/) - [@Youtube list](https://www.youtube.com/watch?v=f1WTYGORU3w&list=PLUVUdDGxXszgq6abyzMsbQ4omDp6UL2J1)_

### Quiz:
What is the minimum original number of nodes needed to assure the election of a new Primary if a node goes down?

 * 1
 * 2
 * 3
 * 5

See [answer](./02_ANSWER_intro_replication.md)




## 3. Replica Set Elections
_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_6_Application_Engineering/52b35b64e2d423678d3b9d3c/)_
_[@Youtube list](https://www.youtube.com/watch?v=WFXSVHO78bQ&list=PLUVUdDGxXszgq6abyzMsbQ4omDp6UL2J1)_

### Quiz:
Which types of nodes can participate in elections of a new primary?

 * Regular replica set members
 * Hidden Members
 * Arbiters
 * Lawyers

See [answer](./03_ANSWER_replica_set_elections.md)



## 4. Write Consistency
_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_6_Application_Engineering/52b35c9ae2d423678d3b9d40/)_
_[@Youtube list](https://www.youtube.com/watch?v=Oqf_Eza-s1M&list=PLUVUdDGxXszgq6abyzMsbQ4omDp6UL2J1)_

### Quiz:
During the time when failover is occurring, can writes successfully complete?

 * Yes
 * No

See [answer](./04_ANSWER_write_consistency.md)




## 5. Creating a Replica Set
_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_6_Application_Engineering/52b35d99e2d423678d3b9d44/)_
_[@Youtube list](http://www.youtube.com/watch?v=flCFVFBRsKI&list=PLUVUdDGxXszgq6abyzMsbQ4omDp6UL2J1)_

### Quiz:
Which command, when issued from the mongo shell, will allow you to read from a secondary?

 * db.isMaster()
 * db.adminCommand({'readPreference':'Secondary'})
 * rs.setStatus("Primary")
 * rs.slaveOk()

See [answer](./05_creating_a_replica_set/ANSWER.md)



 
## 6. Implications of Replication
_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_6_Application_Engineering/52df65c6e2d423744501cfd7/)_
_[@Youtube list](http://www.youtube.com/watch?v=GEViDe9NRMk&list=PLUVUdDGxXszgq6abyzMsbQ4omDp6UL2J1)_





## 7. Replica Set Internals
_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_6_Application_Engineering/52b36671e2d423678d3b9d52/)_
_[@Youtube list](http://www.youtube.com/watch?v=lx0Kj4n4EWM&list=PLUVUdDGxXszgq6abyzMsbQ4omDp6UL2J1)_

### Quiz:
In the video how long did it take to elect a new primary?

 * About three seconds
 * Above 10 seconds
 * About 30 seconds
 * About a minute

See [answer](./07_ANSWER_replica_set_internals.md)





## 8. Failover and Rollback
_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_6_Application_Engineering/52b36898e2d423678d3b9d56/)_
_[@Youtube list](https://www.youtube.com/watch?v=IW1oW_Adlt0&list=PLUVUdDGxXszgq6abyzMsbQ4omDp6UL2J1)_

### Quiz:
What happens if a node comes back up as a secondary after a period of being offline and the oplog has looped on the primary?

 * The entire dataset will be copied from the primary
 * A rollback will occur
 * The new node stays offline (does not re-join the replica set)
 * The new node begins to calculate Pi to a large number of decimal places
 
See [answer](./08_ANSWER_failover_and_rollback.md)






## 9. Connecting to a Replica Set from the Node.js Driver
_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_6_Application_Engineering/52dfeaa8e2d423744501cfd9/)_
_[@Youtube list](https://www.youtube.com/watch?v=H1bzY0ktgEg&list=PLUVUdDGxXszgq6abyzMsbQ4omDp6UL2J1)_

### Example:
 - [app.js](./09_connect_to_replica_set/app.js)

### Quiz:
If you leave a replica set node out of the seedlist within the driver, what will happen?

 * The missing node will not be used by the application.
 * The missing node will be discovered as long as you list at least one valid node.
 * This missing node will be used for reads, but not for writes.
 * The missing node will be used for writes, but not for reads.
 
See [answer](./09_connect_to_replica_set/ANSWER.md)






## 10. Failover in the Node.js Driver
_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_6_Application_Engineering/52dfeae3e2d423744501cfdd/)_
_[@Youtube list](https://www.youtube.com/watch?v=15jBQRolLV4&list=PLUVUdDGxXszgq6abyzMsbQ4omDp6UL2J1)_

### Example:
 - [app.js](./10_failover_in_nodejs/app.js)

### Quiz:

```
db.collection('foo').insert({x:1}, callback);
```

What will happen if this insert happens during a primary election?

 * The insert will immediately succeed and the callback will be called
 * The insert will fail with an error
 * The insert will be buffered until the election completes, then the callback will be called after the operation is sent and a response is received
 * The callback will be called first, then the insert will be buffered until the election completes

See [answer](./10_failover_in_nodejs/ANSWER.md)





## 11. Write Concern
_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_6_Application_Engineering/52dfebbbe2d423744501cfe1/)_
_[@Youtube list](https://www.youtube.com/watch?v=ucqgEUVISqM&list=PLUVUdDGxXszgq6abyzMsbQ4omDp6UL2J1)_

### Example:
 - [app.js](./11_write_concern/app.js)

### Quiz:
What happens if we specify a write concern larger than the number of nodes we currently have up?

 * The write waits forever.
 * The driver throws an error.
 * The server throws an error.
 * The write goes to as many nodes as possible then the driver returns success.

See [answer](./11_write_concern/ANSWER.md)






## 12. Read Preferences

_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_6_Application_Engineering/52dfec0ae2d423744501cfe5/)_
_[@Youtube list](http://www.youtube.com/watch?v=vpbs3SEgmvE&list=PLUVUdDGxXszgq6abyzMsbQ4omDp6UL2J1)_

### Example:
 - [app.js](./12_read_preference/app.js)

### Quiz:
You can configure your applications via the drivers to read from secondary nodes within a replica set. What are the reasons that you might not want to do that? Check all that apply.

 * If your write traffic is significantly greater than your read traffic, you may overwhelm the secondary, which must process all the writes as well as the reads. Replication lag can result.
 * You may not read what you previously wrote to MongoDB.
 * If the secondary hardware has insufficient memory to keep the read working set in memory, directing reads to it will likely slow it down.
 * Reading from a secondary prevents it from being promoted to primary.

See [answer](./12_read_preference/ANSWER.md)





## 13. Network Errors
_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_6_Application_Engineering/52b34d8ee2d42362670d8272/)_
_[@Youtube list](http://www.youtube.com/watch?v=BycNYXFpqhM&list=PLUVUdDGxXszgq6abyzMsbQ4omDp6UL2J1)_

### Quiz:
What are the reasons why an application may receive an error back even if the write was successful. Check all that apply.

 * The network TCP network connection between the application and the server was reset between the time of the write and the time of the getLastError call.
 * The MongoDB server terminates between the write and the getLastError call.
 * The network fails between the time of the write and the time of the getLastError call
 * The write violates a primary key constraint on the collection and must be rolled back.

See [answer](./13_ANSWER_network_errors.md)








## 14. Introduction to Sharding
_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_6_Application_Engineering/52b36e8ee2d423678d3b9d88/)_
_[@Youtube list](http://www.youtube.com/watch?v=_GfDqa1qRl0&list=PLUVUdDGxXszgq6abyzMsbQ4omDp6UL2J1)_

### Quiz:
If the shard key is not include in a find operation and there are 3 shards, each one a replica set with 3 nodes, how many nodes will see the find operation?

 * 1
 * 3
 * 9
 * 6

See [answer](./14_ANSWER_introduction_to_sharding.md)





## 15. Building a Sharded Environment
_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_6_Application_Engineering/52b36ef0e2d423678d3b9d8c/)_
_[@Youtube list](http://www.youtube.com/watch?v=aaYc2W4keF4&list=PLUVUdDGxXszgq6abyzMsbQ4omDp6UL2J1)_

### Example:
 - [init_sharded_env.sh](./15_building_sharded_env/init_sharded_env.sh)

### Quiz:
If you want to build a production system with two shards, each one a replica set with three nodes, how may mongod processes must you start?

 * 2
 * 6
 * 7
 * 9

2 shards has 6 nodes. 3 config nodes

See [answer](./15_building_sharded_env/ANSWER.md)





## 16. Implications of Sharding

_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_6_Application_Engineering/52b370dce2d423678d3b9d93/)_
_[@Youtube list](https://www.youtube.com/watch?v=ig278F60gRA&list=PLUVUdDGxXszgq6abyzMsbQ4omDp6UL2J1)_

### Quiz:
Suppose you wanted to shard the zip code collection after importing it. You want to shard on zip code. What index would be required to allow MongoDB to shard on zip code?

 * An index on zip or a non-multi-key index that starts with zip.
 * No index is required to use zip as the shard key.
 * A unique index on the zip code.
 * Any index that that includes the zip code.

See [answer](./16_ANSWER_implications_of_sharding.md)





## 17. Sharding + Replication
_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_6_Application_Engineering/52b37130e2d423678d3b9d97/)_
_[@Youtube list](https://www.youtube.com/watch?v=gkUCUbM0oEg&list=PLUVUdDGxXszgq6abyzMsbQ4omDp6UL2J1)_

### Quiz:
Suppose you want to run multiple mongos routers for redundancy. What level of the stack will assure that you can failover to a different mongos from within your application?

 * mongod
 * mongos
 * drivers
 * sharding config servers

See [answer](./17_ANSWER_sharding_replication.md)






## 18. Choosing a Shard Key
_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_6_Application_Engineering/52b37184e2d423678d3b9d9b/)_
_[@Youtube list](https://www.youtube.com/watch?v=8q2GB3QSBSI&list=PLUVUdDGxXszgq6abyzMsbQ4omDp6UL2J1)_

### Quiz:
You are building a facebook competitor called footbook that will be a mobile social network of feet. You have decided that your primary data structure for posts to the wall will look like this:

```
{
 'username':'toeguy',
 'posttime':ISODate("2012-12-02T23:12:23Z"),
 "randomthought": "I am looking at my feet right now",
 'visible_to':['friends','family', 'walkers']
 }
```

Thinking about the tradeoffs of shard key selection, select the true statements below.

 * Choosing posttime as the shard key will cause hotspotting as time progresses.
 * Choosing username as the shard key will distribute posts to the wall well across the shards.
 * Choosing visible_to as a shard key is illegal.
 * Choosing posttime as the shard key suffers from low cardinality.

See [answer](./18_ANSWER_choosing_shard_key.md)




## [Homework 6.1](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_6_Application_Engineering/52b372c1e2d423678d3b9d9f/)

Which of the following statements are true about MongoDB replication. Check all that apply.

 * The minimum sensible number of voting nodes to a replica set is three.
 * MongoDB replication is synchronous.
 * The Mongo shell is capable of attaching to a replica set and automatically failing over.
 * By default, using the new MongoClient connection class, w=1 and j=1.
 * The oplog utilizes a capped collection.

See [answer](./HW_6.1_ANSWER.md)




## [Homework 6.2](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_6_Application_Engineering/52b37321e2d423678d3b9da2/)

Let's suppose you have a five member replica set and want to assure that writes are committed to the journal and are acknowledged by at least 3 nodes before you proceed forward. What would be the appropriate settings for w and j?

 * w=1, j=1
 * w="majority", j=1
 * w=3, j=0
 * w=5, j=1
 * w=1,j=3

See [answer](./HW_6.2_ANSWER.md)




## [Homework 6.3](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_6_Application_Engineering/52b37369e2d423678d3b9da5/)

Which of the following statements are true about choosing and using a shard key:

 * The shard key must be unique
 * There must be a index on the collection that starts with the shard key.
 * Mongo can not enforce unique indexes on a sharded collection other than the shard key itself.
 * Any update that does not contain the shard key will be sent to all shards.
 * You can change the shard key on a collection if you desire.

See [answer](./HW_6.3_ANSWER.md)



## [Homework 6.4](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_6_Application_Engineering/52b373b3e2d423678d3b9da8/)

You have a sharded system with three shards and have sharded the collections "grades" in the "test" database across those shards. The output of sh.status() when connected to mongos looks like this:

```
mongos> sh.status()
--- Sharding Status --- 
  sharding version: { "_id" : 1, "version" : 3 }
  shards:
 {  "_id" : "s0",  "host" : "s0/localhost:37017,localhost:37018,localhost:37019" }
 {  "_id" : "s1",  "host" : "s1/localhost:47017,localhost:47018,localhost:47019" }
 {  "_id" : "s2",  "host" : "s2/localhost:57017,localhost:57018,localhost:57019" }
  databases:
 {  "_id" : "admin",  "partitioned" : false,  "primary" : "config" }
 {  "_id" : "test",  "partitioned" : true,  "primary" : "s0" }
  test.grades chunks:
    s1 4
    s0 4
    s2 4
   { "student_id" : { $minKey : 1 } } -->> { "student_id" : 0 } on : s1 Timestamp(12000, 0) 
   { "student_id" : 0 } -->> { "student_id" : 2640 } on : s0 Timestamp(11000, 1) 
   { "student_id" : 2640 } -->> { "student_id" : 91918 } on : s1 Timestamp(10000, 1) 
   { "student_id" : 91918 } -->> { "student_id" : 176201 } on : s0 Timestamp(4000, 2) 
   { "student_id" : 176201 } -->> { "student_id" : 256639 } on : s2 Timestamp(12000, 1) 
   { "student_id" : 256639 } -->> { "student_id" : 344351 } on : s2 Timestamp(6000, 2) 
   { "student_id" : 344351 } -->> { "student_id" : 424983 } on : s0 Timestamp(7000, 2) 
   { "student_id" : 424983 } -->> { "student_id" : 509266 } on : s1 Timestamp(8000, 2) 
   { "student_id" : 509266 } -->> { "student_id" : 596849 } on : s1 Timestamp(9000, 2) 
   { "student_id" : 596849 } -->> { "student_id" : 772260 } on : s0 Timestamp(10000, 2) 
   { "student_id" : 772260 } -->> { "student_id" : 945802 } on : s2 Timestamp(11000, 2) 
   { "student_id" : 945802 } -->> { "student_id" : { $maxKey : 1 } } on : s2 Timestamp(11000, 3) 
```
If you ran the query

```
use test
db.grades.find({'student_id':530289})
```

Which shards would be involved in answering the query?

 * s0,s1 and s2
 * s0
 * s1
 * s2

See [answer](./HW_6.4_ANSWER.md)





## [Homework 6.5](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_6_Application_Engineering/52dfec53e2d423744501cfe9/)

In this homework you will build a small replica set on your own computer. We will check that it works with validate.js, which you should download from the Download Handout link. 

Create three directories for the three mongod processes.

On **unix or mac**, this could be done as follows:

```
mkdir -p /data/rs1 /data/rs2 /data/rs3
```

Now start three mongo instances as follows. Note that are three commands. The browser is probably wrapping them visually.

```
mongod --replSet m101 --logpath "1.log" --dbpath /data/rs1 --port 27017 --smallfiles --fork

mongod --replSet m101 --logpath "2.log" --dbpath /data/rs2 --port 27018 --smallfiles --fork

mongod --replSet m101 --logpath "3.log" --dbpath /data/rs3 --port 27019 --smallfiles --fork
```

**Windows users**: Omit -p from mkdir. Also omit --fork and use start mongod with Windows compatible paths (i.e. back slashes "\") for the --dbpath argument (e.g; C:\data\rs1). 

Now connect to a mongo shell and make sure it comes up

```
mongo --port 27017
```

Now you will create the replica set. Type the following commands into the mongo shell:

```
config = { _id: "m101", members:[
          { _id : 0, host : "localhost:27017"},
          { _id : 1, host : "localhost:27018"},
          { _id : 2, host : "localhost:27019"} ]
};
rs.initiate(config);
```

At this point, the replica set should be coming up. You can type

```
rs.status()
```

to see the state of replication. 

```
$ mongo --port 27018
MongoDB shell version: 2.4.6
connecting to: 127.0.0.1:27018/test
m101:PRIMARY> rs.status()
{
	"set" : "m101",
	"date" : ISODate("2014-02-21T00:39:01Z"),
	"myState" : 1,
	"members" : [
		{
			"_id" : 0,
			"name" : "localhost:27017",
			"health" : 1,
			"state" : 2,
			"stateStr" : "SECONDARY",
			"uptime" : 180,
			"optime" : Timestamp(1392942961, 1),
			"optimeDate" : ISODate("2014-02-21T00:36:01Z"),
			"lastHeartbeat" : ISODate("2014-02-21T00:39:01Z"),
			"lastHeartbeatRecv" : ISODate("2014-02-21T00:39:01Z"),
			"pingMs" : 0,
			"syncingTo" : "localhost:27018"
		},
		{
			"_id" : 1,
			"name" : "localhost:27018",
			"health" : 1,
			"state" : 1,
			"stateStr" : "PRIMARY",
			"uptime" : 507,
			"optime" : Timestamp(1392942961, 1),
			"optimeDate" : ISODate("2014-02-21T00:36:01Z"),
			"self" : true
		},
		{
			"_id" : 2,
			"name" : "localhost:27019",
			"health" : 1,
			"state" : 2,
			"stateStr" : "SECONDARY",
			"uptime" : 180,
			"optime" : Timestamp(1392942961, 1),
			"optimeDate" : ISODate("2014-02-21T00:36:01Z"),
			"lastHeartbeat" : ISODate("2014-02-21T00:39:01Z"),
			"lastHeartbeatRecv" : ISODate("2014-02-21T00:39:00Z"),
			"pingMs" : 0,
			"syncingTo" : "localhost:27018"
		}
	],
	"ok" : 1
}
```

Now run validate.js to confirm that it works.

```
node validate.js
```

Validate connects to your local replica set and checks that it has three nodes. Type the validation code below.

```
XdYfY6aqjqS3ik35qS6v
```
