# [<< Week 6](../README.md) < 9. Connecting to a Replica Set from the Node.js Driver - Answer
_[@Youtube video answer](https://www.youtube.com/watch?v=FUXB7GrDeA0&list=PLUVUdDGxXszgq6abyzMsbQ4omDp6UL2J1)_

**If you leave a replica set node out of the seedlist within the driver, what will happen?**

 * The missing node will not be used by the application.
 * **The missing node will be discovered as long as you list at least one valid node.**
 * This missing node will be used for reads, but not for writes.
 * The missing node will be used for writes, but not for reads.

_Answer is: **The missing node will be discovered as long as you list at least one valid node**_