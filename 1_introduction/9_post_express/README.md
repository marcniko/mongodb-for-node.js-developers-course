# [Express handling POST Requests](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_1_Introduction/52549a53e2d4231cc608400b/)
_https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_1_Introduction/52549a53e2d4231cc608400b/_

## Install packages
```
npm install express consolidate swig
```
see middlewares: [http://expressjs.com/](http://expressjs.com/)

## Run node.js example
```
nodejs app.js
=> Express server listening on port 3000
```

see: [http://localhost:3000](http://localhost:3000)
=> 
```
What is your favorite fruit?

 o apple

 o orange

 o banana

 o peach

[submit]
```

=> http://localhost:3000/favorite_fruit
```
Your favorite fruit is <fruit_selected>
```

Submit without selecting any fruit
=> 
```
Error: Please choose a fruit!
```
