# [Homework: Homework 1.2](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_1_Introduction/525841e5e2d4233537765333/)
_https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_1_Introduction/525841e5e2d4233537765333/_

Your assignment for this part of the homework is to install the mongodb driver for Node.js and run the test application.To do this, first download the hw1-2.zip from Download Handout link, uncompress and change into the hw1-2 directory:
```
cd hw1-2
```

Then install the mongodb driver:
```
npm install mongodb
```

This should create a "node_modules" directory. Now run the application to get the answer to hw1-2:
```
nodejs app.js
```

If you have the mongodb driver installed correctly, this will print out the message 'Answer: ' followed by some additional text. Write that text in the text box below.

```
I like kittens
```
