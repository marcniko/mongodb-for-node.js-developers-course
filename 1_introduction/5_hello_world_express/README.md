# [Hello World using Express](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_1_Introduction/52549988e2d4231cc6084007/)
_https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_1_Introduction/52549988e2d4231cc6084007/_

## Install package Express
```
npm install express
```

## Run node.js example
```
node app.js

=> Express server started on port 8080
```

see:
[http://localhost:8080](http://localhost:8080)
[http://localhost:8080/notfoundWhatEverYouWantNotExists](http://localhost:8080/notfoundWhatEverYouWantNotExists)
