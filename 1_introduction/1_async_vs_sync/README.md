# [Asynchronous vs synchronous](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_1_Introduction/525498a5e2d4231cc6084003/)
_https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_1_Introduction/525498a5e2d4231cc6084003/_

## Installation Node Packaged Modules (npmjs.org)
```
sudo apt-get install npm
```

## Installation mongodb package for node.js
```
npm install mongodb
```

## Run mongo shell example
```
mongo script.js
```

## Run node.js example
```
node app.js
```
