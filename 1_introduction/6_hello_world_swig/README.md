# [Hello World using Express and Swig](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_1_Introduction/5254999de2d4231cc6084008/)
_https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_1_Introduction/5254999de2d4231cc6084008/_

## Install package Express and Swig
```
npm install express consolidate swig
```

## Run node.js example
```
node app.js

=> Express server started on port 8080
```

see:
[http://localhost:8080](http://localhost:8080)
[http://localhost:8080/notfoundWhatEverYouWantNotExists](http://localhost:8080/notfoundWhatEverYouWantNotExists)
