# [Homework: Homework 1.3](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_1_Introduction/525843fce2d4233537765334/)
_https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_1_Introduction/525843fce2d4233537765334/_

Your assignment for this part of the homework is to install the mongodb driver for Node.js, Express, and other required dependencies and run the test application. This homework is meant to give you practice using the "package.json" file, which will include some of the code that we provide.

To do this, first download the hw1-3.zip from Download Handout link, uncompress and change into the hw1-3 directory:

```
cd hw1-3
```

Then install all the dependencies listed in the 'package.json' file. Calling 'npm install' with no specific package tells npm to look for 'package.json':

```
npm install
```

This should create a "node_modules" directory with all the dependencies. Now run the application to get the answer to hw1-3:

```
node app.js
```

If you have all the dependencies installed correctly, this will print the message 'Express server started on port 8080'. Navigate to 'localhost:8080' in a browser and write the text that is displayed on that page in the text box below.

```
Hello, Agent 007.
```
