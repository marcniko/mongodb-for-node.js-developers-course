# [Express handling GET Requests](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_1_Introduction/525499dce2d4231cc608400a/)
_https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_1_Introduction/525499dce2d4231cc608400a/_

## Install packages
```
npm install express consolidate swig
```

## Run node.js example
```
nodejs app.js
=> Express server listening on port 3000
```

see: [http://localhost:3000](http://localhost:3000)
=> 
```
Cannot GET /
```

see: [http://localhost:3000/marc](http://localhost:3000/marc)

=>
```
Hello, marc, here are your GET variables:
```

[http://localhost:3000/marc?getvar1=primera&getvar2=segona](http://localhost:3000/marc?getvar1=primera&getvar2=segona)
=> 
```
Hello, marc, here are your GET variables:
 - primera
 - segona
```
