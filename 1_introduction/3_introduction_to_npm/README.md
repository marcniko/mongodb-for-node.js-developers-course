# [Introduction to npm](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_1_Introduction/525498e0e2d4231cc6084005/)
_https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_1_Introduction/525498e0e2d4231cc6084005/_

## Run node.js example
```
node app.js

 => Cannot find module 'express'... 'consolidate'
```

## Installation packages for node.js using package.json
```
npm install
```

```
node app.js
```
 => works requires!
