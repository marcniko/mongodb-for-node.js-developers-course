# [Hello World using Express, Swig and MongoDB](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_1_Introduction/5254999de2d4231cc6084009/)
_https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_1_Introduction/5254999de2d4231cc6084009/_

## Install packages
```
npm install mongodb express consolidate swig
```

## Run node.js example
```
nodejs app.js
=> Express server started on port 8080

```
see:
[http://localhost:8080](http://localhost:8080)
[http://localhost:8080/notfoundWhatEverYouWantNotExists](http://localhost:8080/notfoundWhatEverYouWantNotExists)

## Insert doc to MongoDB
```
mongo
use course
db.hello_mongo_express.insert({"name": "MongoDB"})
```
## Run again node.js app
```
nodejs app.js
```
see: [http://localhost:8080](http://localhost:8080)
```
Hello, MongoDB!
```
