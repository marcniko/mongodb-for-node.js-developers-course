# [<< Week 5](../README.md) < 13. $project - Answer
_[@Youtube video answer](http://www.youtube.com/watch?v=IGN0lXg-kJ0&list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N)_

_Answer is:_
```
db.zips.aggregate([
    {$project:
     {
      _id: 0,
      'city': {$toLower:"$city"},
      'pop': 1,
      'state': 1,
      'zip': '$_id'
     }
    }
])
```

See [quiz_using_project.js](./quiz_using_project.js)
```
mongo < 13_project/quiz_using_project.js
```