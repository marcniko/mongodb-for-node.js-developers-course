# [<< Week 5](../README.md) < 20. Double $unwind - Answer
_[@Youtube video answer](http://www.youtube.com/watch?v=pziFq1oVRI4&list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N)_

**Can you reverse the effects of a double unwind (2 unwinds in a row) in our inventory collection (shown in the lesson ) with the $push operator?**

 * **Yes**
 * No

_Answer is: Yes