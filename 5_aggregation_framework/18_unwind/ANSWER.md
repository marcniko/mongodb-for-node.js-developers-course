# [<< Week 5](../README.md) < 18. $unwind - Answer
_[@Youtube video answer](http://www.youtube.com/watch?v=Xfl3m7wz8ts&list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N)_

**How many documents will you wind up with?**

   * 2
   * 4
   * **6**
   * 9

_Answer is: 6_

Check the results quering it, see [quiz_unwind.js](./quiz_unwind.js)
```
mongo < 18_unwind/quiz_unwind.js
```