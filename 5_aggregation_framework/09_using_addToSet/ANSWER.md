# [<< Week 5](./README.md) < 9. Using addToSet - Answer
_[@Youtube video answer](http://www.youtube.com/watch?v=zGcTxUQuLGE&list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N)_

**Write an aggregation query that will return the postal codes that cover each city. The results should look like this:**

```
		{
			"_id" : "CENTREVILLE",
			"postal_codes" : [
				"22020",
				"49032",
				"39631",
				"21617",
				"35042"
			]
		},
```

_Answer:_
```
db.zips.aggregate([{$group: {"_id": "$city", "postal_codes": {"$addToSet": "$_id"}}}])
```