# [<< Week 5](../README.md) < 14. $match - Answer
_[@Youtube video answer](http://www.youtube.com/watch?v=CHGiumXE-pU&list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N)_

_Answer is:_
```
db.zips.aggregate([
    {$match:
        {pop: {"$gt": 100000}}
    }
])
```

See [quiz_using_match.js](./quiz_using_match.js)
```
mongo < 14_match/quiz_using_match.js
```