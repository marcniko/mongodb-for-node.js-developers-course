# [<< Week 5](../README.md) < 7. Using $sum - Answer
_[@Youtube video answer](https://www.youtube.com/watch?feature=player_embedded&v=ATO_s_Ah08o)_

**Answer is:**
```
db.zips.aggregate([
	{$group:
		{
		 _id: "$state",
		 population: {$sum:"$pop"}
		}
	}
])
```

See: [quiz_sum_by_state.js](./quiz_sum_by_state.js)
```
mongo < 7_using_sum/quiz_sum_by_state.js
```