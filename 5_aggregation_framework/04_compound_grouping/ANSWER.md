# [<< Week 5](../README.md) < 4. Compound Grouping - Answer
_[@Youtube video answer](https://www.youtube.com/watch?v=bIlwnlYBRA0&list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N)_


**How many documents will be in the result set?**

 * 2
 * 3
 * 4
 * **5**
 * 6

_Answer is: 5 _

```
{ "_id" : "moe" : 1, "larry" : 1, "curly" : 1 }
{ "_id" : "moe" : 2, "larry" : 2, "curly" : 1 }
{ "_id" : "moe" : 3, "larry" : 3, "curly" : 1 }
{ "_id" : "moe" : 3, "larry" : 3, "curly" : 2 }
{ "_id" : "moe" : 3, "larry" : 5, "curly" : 3 }
```

`{ "moe" : 3, "larry" : 3, "curly" : 2 }`, this is the unic duplicated combination (a,b,c).
