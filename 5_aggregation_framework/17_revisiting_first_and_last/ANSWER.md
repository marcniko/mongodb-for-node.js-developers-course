# [<< Week 5](../README.md) < 17. Revisiting $first and $last - Answer
_[@Youtube video answer](http://www.youtube.com/watch?v=WBWb0ssDVeY&list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N)_

**What would be the value of c in the result from this aggregation query?**

```
db.fun.aggregate([
    {$match:{a:0}},
    {$sort:{c:-1}},
    {$group:{_id:"$a", c:{$first:"$c"}}}
])
```

 * 21
 * **54**
 * 97
 * 5

_Answer is: 54_