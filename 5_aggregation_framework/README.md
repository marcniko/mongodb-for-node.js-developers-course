# Week 5: Aggregation Framework
_[List videos @Youtube](https://www.youtube.com/playlist?list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N)_

## 0. Introduction to Week 5
_[Video introduction](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_5_Aggregation_Framework/52aa33cee2d4232c54a18ad8/) - [@Youtube list](https://www.youtube.com/watch?v=EaEIHm3CMQM&list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N)_

## [1. Simple Aggregation Example](./01_simple_aggregation_example/README.md)
_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_5_Aggregation_Framework/52aa3411e2d4232c54a18ada/) - [@Youtube list](https://www.youtube.com/watch?v=T2BX-LZOYsY&list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N)_

## 2. The Aggregation Pipeline
_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_5_Aggregation_Framework/52aa350fe2d4232c54a18ade/) - [@Youtube list](https://www.youtube.com/watch?v=mcnGfI69rRc&list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N)_

### Quiz: Which of the following are stages in the aggregation pipeline. Check all that apply.

 1. Match
 1. Transpose
 1. Group
 1. Skip
 1. Limit
 1. Sort
 1. Project
 1. Unwind

See [answer](./02_aggregation_pipeline_ANSWER.md)





## 3. Simple Example Expanded
_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_5_Aggregation_Framework/52aa360ee2d4232c54a18ae2/) - [@Youtube list](https://www.youtube.com/watch?v=3lEpnMcfpCs&list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N)_

If you have the following collection of stuff:
```
> db.stuff.find()
{ "_id" : ObjectId("50b26f9d80a78af03b5163c8"), "a" : 1, "b" : 1, "c" : 1 }
{ "_id" : ObjectId("50b26fb480a78af03b5163c9"), "a" : 2, "b" : 2, "c" : 1 }
{ "_id" : ObjectId("50b26fbf80a78af03b5163ca"), "a" : 3, "b" : 3, "c" : 1 }
{ "_id" : ObjectId("50b26fcd80a78af03b5163cb"), "a" : 3, "b" : 3, "c" : 2 }
{ "_id" : ObjectId("50b26fd380a78af03b5163cc"), "a" : 3, "b" : 5, "c" : 3 }
```

and you perform the following aggregation:
```
db.stuff.aggregate([{$group:{_id:'$c'}}])
```

#### Quiz: How many documents will be in the result set from aggregate?

 * 1
 * 2
 * 3
 * 4
 * 5

See [answer](./03_simple_example_expanded_ANSWER.md)




## 4. Compound Grouping
_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_5_Aggregation_Framework/52aa3666e2d4232c54a18ae6/) - [@Youtube list](https://www.youtube.com/watch?v=LPFVRrag2Zg&list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N)_

## Examples:

 * [Simple example](./04_compound_grouping/simple_example1.js)
 * [Compound example](./04_compound_grouping/compound1.js)

#### Quiz:
Given the following collection:
```
> db.stuff.find()
{ "_id" : ObjectId("50b26f9d80a78af03b5163c8"), "a" : 1, "b" : 1, "c" : 1 }
{ "_id" : ObjectId("50b26fb480a78af03b5163c9"), "a" : 2, "b" : 2, "c" : 1 }
{ "_id" : ObjectId("50b26fbf80a78af03b5163ca"), "a" : 3, "b" : 3, "c" : 1 }
{ "_id" : ObjectId("50b26fcd80a78af03b5163cb"), "a" : 3, "b" : 3, "c" : 2 }
{ "_id" : ObjectId("50b26fd380a78af03b5163cc"), "a" : 3, "b" : 5, "c" : 3 }
{ "_id" : ObjectId("50b27f7080a78af03b5163cd"), "a" : 3, "b" : 3, "c" : 2 }
```

And the following aggregation query:
```
db.stuff.aggregate([{$group:
             {_id:
              {'moe':'$a',
               'larry':'$b',
               'curly':'$c'
              }
             }
            }])
```

**How many documents will be in the result set?**

 * 2
 * 3
 * 4
 * 5
 * 6

See [answer](./04_compound_grouping/ANSWER.md)




## 5. Using a document for _id
_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_5_Aggregation_Framework/52aa370de2d4232c54a18aea/) - [@Youtube list](https://www.youtube.com/watch?v=zoN4cj_XQzY&list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N)_

Multiple key for *_id* must be unique:

![05_unique_multiple_key_for_id.png](https://bitbucket.org/marcniko/mongodb-for-node.js-course/raw/c9503cfdfb100bbed76bdada618f2ac17298748d/week5_aggregation_framework/05_unique_multiple_key_for_id.png)




## 6. Aggregation Expressions
_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_5_Aggregation_Framework/52aa3727e2d4232c54a18aec/) - [@Youtube list](https://www.youtube.com/watch?v=L4G14MTfTgQ&list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N)_

### Quiz: Which of the following aggregation expressions must be used in conjunction with a sort to make any sense?

 * $addToSet
 * $first
 * $last
 * $max
 * $min
 * $avg
 * $push
 * $sum

See [answer](./06_aggregation_expressions_ANSWER.md)




## 7. Using $sum
_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_5_Aggregation_Framework/52aa378fe2d4232c54a18af0/) - [@Youtube list](https://www.youtube.com/watch?v=L4G14MTfTgQ&list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N)_

## Import zips collection (http://media.mongodb.org/zips.json):
```
mongoimport -d agg -c zips < zips.json
```

### Example:
 * [using_sum.js](./07_using_sum/using_sum.js)

```
mongo < 07_using_sum/using_sum.js
```

### Quiz:
This problem, and some after it, use the zips collection from media.mongodb.org/zips.json. You don't need to download it, but you can if you want, allowing you to test your queries within MongoDB. You can import, once downloaded, using mongoimport
Suppose we have a collection of populations by postal code. The postal codes in are in the _id field, and are therefore unique. Documents look like this:

```
{
    "city" : "CLANTON",
    "loc" : [
        -86.642472,
        32.835532
    ],
    "pop" : 13990,
    "state" : "AL",
    "_id" : "35045"
}
```

For students outside the United States, there are 50 non-overlapping states in the US with two letter abbreviations such as NY and CA. In addition, the capital of Washington is within an area designated the District of Columbia, and carries the abbreviation DC. For purposes of the mail, the postal service considers DC to be a "state." So in this dataset, there are 51 states. We call postal codes "zip codes." A city may overlap several zip codes.
Write an aggregation query to sum up the population (pop) by state and put the result in a field called population. Don't use a compound _id key (you don't need one and the quiz checker is not expecting one). The collection name is zips. so something along the lines of db.zips.aggregate...

See [answer](./07_using_sum/ANSWER.md)




## 8. Using $avg
_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_5_Aggregation_Framework/52aa38a4e2d4232c54a18af4/) - [@Youtube list](https://www.youtube.com/watch?v=7UWOK8rWf1w&list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N)_

### Example:
 * [using_avg.js](./08_using_avg/using_avg.js)
```
mongo < 08_using_avg/using_avg.js
```

### Quiz:
This problem uses the same dataset as we described in using $sum quiz and you should review that quiz if you did not complete it.
Given population data by zip code (postal code) that looks like this:

```
{
    "city" : "FISHERS ISLAND",
    "loc" : [
        -72.017834,
        41.263934
    ],
    "pop" : 329,
    "state" : "NY",
    "_id" : "06390"
}
```

Write an aggregation expression to calculate the average population of a zip code (postal code) by state. As before, the postal code is in the _id field and is unique. The collection is assumed to be called "zips" and you should name the key in the result set "average_pop".

See [answer](./08_using_avg/ANSWER.md)



## 9. Using $addToSet
_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_5_Aggregation_Framework/52aa390ce2d4232c54a18af8/) - [@Youtube list](https://www.youtube.com/watch?v=y2FD4R3in5U&list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N)_

### Example:
 * [using_addToSet.js](./09_using_addToSet/using_addToSet.js)
```
mongo < 09_using_addToSet/using_addToSet.js
```

### Quiz:
This problem uses the same zip code data as the $using sum quiz. See that quiz for a longer explanation.
Suppose we population by zip code (postal code) data that looks like this (putting in a query for the zip codes in Palo Alto)

```
> db.zips.find({state:"CA",city:"PALO ALTO"})
{ "city" : "PALO ALTO", "loc" : [ -122.149685, 37.444324 ], "pop" : 15965, "state" : "CA", "_id" : "94301" }
{ "city" : "PALO ALTO", "loc" : [ -122.184234, 37.433424 ], "pop" : 1835, "state" : "CA", "_id" : "94304" }
{ "city" : "PALO ALTO", "loc" : [ -122.127375, 37.418009 ], "pop" : 24309, "state" : "CA", "_id" : "94306" }
```

Write an aggregation query that will return the postal codes that cover each city. The results should look like this:

```
        {
            "_id" : "CENTREVILLE",
            "postal_codes" : [
                "22020",
                "49032",
                "39631",
                "21617",
                "35042"
            ]
        },
```

Again the collection will be called zips. You can deduce what your result column names should be from the above output. (ignore the issue that a city may have the same name in two different states and is in fact two different cities in that case - for eg Springfield, MO and Springfield, MA)

See [answer](./09_using_addToSet/ANSWER.md)



## 10. Using $push
_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_5_Aggregation_Framework/52aa3951e2d4232c54a18afc/) - [@Youtube list](http://www.youtube.com/watch?v=gjIVUFufx3A&list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N)_

### Example:
 * [using_push.js](./10_using_push/using_push.js)


### Quiz:
Given the zipcode dataset (explained more fully in the using $sum quiz) that has documents that look like this:

```
> db.zips.findOne()
{
    "city" : "ACMAR",
    "loc" : [
        -86.51557,
        33.584132
    ],
    "pop" : 6055,
    "state" : "AL",
    "_id" : "35004"
}
```

Would you expect the following two queries to produce the same result or different results?

```
db.zips.aggregate([{"$group":{"_id":"$city", "postal_codes":{"$push":"$_id"}}}])
```
```
db.zips.aggregate([{"$group":{"_id":"$city", "postal_codes":{"$addToSet":"$_id"}}}])
```

 * Same result
 * Different Result

See [answer](./10_using_push/ANSWER.md)



## 11. Using $max and $min
_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_5_Aggregation_Framework/52aa399de2d4232c54a18b00/) - [@Youtube list](http://www.youtube.com/watch?v=BYoNX4trjOQ&list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N)_

### Example:
 * [using_max.js](./11_using_max/using_max.js)
```
mongo < 11_using_max/using_max.js
```

### Quiz:
Again thinking about the zip code database, write an aggregation query that will return the population of the postal code in each state with the highest population. It should return output that looks like this:

```
        {
            "_id" : "WI",
            "pop" : 57187
        },
        {
            "_id" : "WV",
            "pop" : 70185
        },
..and so on
```

Once again, the collection is named zips.

See [answer](./11_using_max/ANSWER.md)




## 12. Double group stages
_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_5_Aggregation_Framework/52aa3bb8e2d4232c54a18b04/) - [@Youtube list](http://www.youtube.com/watch?v=ET4ubwQTTos&list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N)_

## Examples:
 * [simple_group.js](./12_double_group_stages/simple_group.js)
 * [double_group.js](./12_double_group_stages/double_group.js)

### Quiz:
Given the following collection:

```
> db.fun.find()
{ "_id" : 0, "a" : 0, "b" : 0, "c" : 21 }
{ "_id" : 1, "a" : 0, "b" : 0, "c" : 54 }
{ "_id" : 2, "a" : 0, "b" : 1, "c" : 52 }
{ "_id" : 3, "a" : 0, "b" : 1, "c" : 17 }
{ "_id" : 4, "a" : 1, "b" : 0, "c" : 22 }
{ "_id" : 5, "a" : 1, "b" : 0, "c" : 5 }
{ "_id" : 6, "a" : 1, "b" : 1, "c" : 87 }
{ "_id" : 7, "a" : 1, "b" : 1, "c" : 97 }
```

And the following aggregation query
```
db.fun.aggregate([{$group:{_id:{a:"$a", b:"$b"}, c:{$max:"$c"}}}, {$group:{_id:"$_id.a", c:{$min:"$c"}}}])
```

What values are returned?

 * 17 and 54
 * 97 and 21
 * 54 and 5
 * 52 and 22

See [answer](./12_double_group_stages/ANSWER.md)





## 13. $project
_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_5_Aggregation_Framework/52aa3c26e2d4232c54a18b08/) - [@Youtube list](http://www.youtube.com/watch?v=yi-ySSNO8Ao&list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N&index=26)_


### Example:
 * [reshape_products.js](./13_project/reshape_products.js)

### Quiz:
Write an aggregation query with a single projection stage that will transform the documents in the zips collection from this:

```
{
    "city" : "ACMAR",
    "loc" : [
        -86.51557,
        33.584132
    ],
    "pop" : 6055,
    "state" : "AL",
    "_id" : "35004"
}
```

to documents in the result set that look like this:

```
{
    "city" : "acmar",
    "pop" : 6055,
    "state" : "AL",
    "zip" : "35004"
}
```

So that the checker works properly, please specify what you want to do with the _id key as the first item. The other items should be ordered as above. As before, assume the collection is called zips. You are running only the projection part of the pipeline for this quiz.

_A few facts not mentioned in the lesson that you will need to know to get this right: If you don't mention a key, it is not included, except for _id, which must be explicitly suppressed. If you want to include a key exactly as it is named in the source document, you just write key:1, where key is the name of the key. You will probably get more out of this quiz is you download the zips.json file and practice in the shell. zips.json link is in the using $sum quiz_

See [answer](./13_project_ANSWER.md)





## 14. $match
_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_5_Aggregation_Framework/52aa3c71e2d4232c54a18b0c/) - [@Youtube list](http://www.youtube.com/watch?v=qohLRL8k0So&list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N&index=28)_

## Examples:
 * [match.js](./14_match/match.js)
 * [match_and_group.js](./14_match/match_and_group.js)
 * [match_group_and_project.js](./14_match/match_group_and_project.js)
```
mongo < 14_match/match.js
mongo < 14_match/match_and_group.js
mongo < 14_match/match_group_and_project.js
```


### Quiz:
Again, thinking about the zipcode collection, write an aggregation query with a single match phase that filters for zipcodes with greater than 100,000 people. You may need to look up the use of the $gt operator in the MongoDB docs.

Assume the collection is called zips.

See [answer](./14_match/ANSWER.md)





## 15. $sort
_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_5_Aggregation_Framework/52aa3cc9e2d4232c54a18b10/) - [@Youtube list](http://www.youtube.com/watch?v=TW1KByIuns4&list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N)_

### Example:
 * [sort.js](./15_sort/sort.js)
```
mongo < 15_sort/sort.js
```

### Quiz:
Again, considering the zipcode collection, which has documents that look like this,

```
{
    "city" : "ACMAR",
    "loc" : [
        -86.51557,
        33.584132
    ],
    "pop" : 6055,
    "state" : "AL",
    "_id" : "35004"
}
```

Write an aggregation query with just a sort stage to sort by (state, city), both ascending. Assume the collection is called zips.

See [answer](./15_sort/ANSWER.md)





## 16. $limit and $skip
_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_5_Aggregation_Framework/52aa3d0fe2d4232c54a18b19/) - [@Youtube list](http://www.youtube.com/watch?v=AdxoEzVqdtc&list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N)_

### Example:
 * [limit.js](./16_limit_and_skip/limit.js)
```
mongo < 16_limit_and_skip/limit.js
```

### Quiz:
Suppose you change the order of skip and limit in the query shown in the lesson, to look like this:

```
db.zips.aggregate([
    {$match:
        {
        state:"NY"
        }
    },
    {$group:
        {
        _id: "$city",
        population: {$sum:"$pop"},
        }
    },
    {$project:
        {
        _id: 0,
        city: "$_id",
        population: 1,
        }
    },
    {$sort:
        {
        population:-1
        }
    },
    {$limit: 5},
    {$skip: 10}
])
```

How many documents do you think will be in the result set?

 * 10
 * 5
 * 0
 * 100

See [answer](./16_limit_and_skip/ANSWER.md)





## 17. Revisiting $first and $last
_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_5_Aggregation_Framework/52aa3da6e2d4232c54a18b1d/) - [@Youtube list](http://www.youtube.com/watch?v=O1zeqAxdUgk&list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N)_

## Examples:
 * [first_phase1.js](./17_revisiting_first_and_last/first_phase1.js)
 * [first_phase2.js](./17_revisiting_first_and_last/first_phase2.js)
 * [first_phase3.js](./17_revisiting_first_and_last/first_phase3.js)
 * [first.js](./17_revisiting_first_and_last/first.js)
```
mongo < 17_revisiting_first_and_last/first_phase1.js
mongo < 17_revisiting_first_and_last/first_phase2.js
mongo < 17_revisiting_first_and_last/first_phase3.js
mongo < 17_revisiting_first_and_last/first.js
```

### Quiz:
Given the following collection:

```
> db.fun.find()
{ "_id" : 0, "a" : 0, "b" : 0, "c" : 21 }
{ "_id" : 1, "a" : 0, "b" : 0, "c" : 54 }
{ "_id" : 2, "a" : 0, "b" : 1, "c" : 52 }
{ "_id" : 3, "a" : 0, "b" : 1, "c" : 17 }
{ "_id" : 4, "a" : 1, "b" : 0, "c" : 22 }
{ "_id" : 5, "a" : 1, "b" : 0, "c" : 5 }
{ "_id" : 6, "a" : 1, "b" : 1, "c" : 87 }
{ "_id" : 7, "a" : 1, "b" : 1, "c" : 97 }
```

What would be the value of c in the result from this aggregation query?

```
db.fun.aggregate([
    {$match:{a:0}},
    {$sort:{c:-1}},
    {$group:{_id:"$a", c:{$first:"$c"}}}
])
```

 * 21
 * 54
 * 97
 * 5

See [answer](./17_revisiting_first_and_last/ANSWER.md)





## 18. $unwind
_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_5_Aggregation_Framework/52aa3df4e2d4232c54a18b21/) - [@Youtube list](http://www.youtube.com/watch?v=E4aYOQPeQvI&list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N)_

### Example:
 * [unwind.js](./18_unwind/unwind.js)
```
mongo < 18_unwind/unwind.js
```

### Quiz:
Suppose you have the following collection:

```
db.people.find()
{ "_id" : "Barack Obama", "likes" : [ "social justice", "health care", "taxes" ] }
{ "_id" : "Mitt Romney", "likes" : [ "a balanced budget", "corporations", "binders full of women" ] }
```

And you unwind the "likes" array of each document. How many documents will you wind up with?

 * 2
 * 4
 * 6
 * 9

See [answer](./18_unwind/ANSWER.md)





## 19. $unwind example
_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_5_Aggregation_Framework/52aa3e46e2d4232c54a18b25/) - [@Youtube list](http://www.youtube.com/watch?v=XiWCJr4Lqag&list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N)_

### Example:
 * [blog_tags.js](./19_unwind_example/blog_tags.js)
```
mongo < 19_unwind_example/blog_tags.js
```

### Quiz:
Which grouping operator will enable to you to reverse the effects of an unwind?

 * $sum
 * $addToSet
 * $push
 * $first

See [answer](./19_unwind_example/ANSWER.md)




## 20. Double $unwind
_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_5_Aggregation_Framework/52aa3ec0e2d4232c54a18b29/) - [@Youtube list]()_

## Examples:
 * [double_unwind.js](./20_double_unwind/double_unwind.js)
 * [reversing_double_unwind.js](./20_double_unwind/reversing_double_unwind.js)
 * [reversing_double_unwind2.js](./20_double_unwind/reversing_double_unwind2.js)
```
mongo < 20_double_unwind/double_unwind.js
mongo < 20_double_unwind/reversing_double_unwind.js
mongo < 20_double_unwind/reversing_double_unwind2.js
```

### Quiz:
Can you reverse the effects of a double unwind (2 unwinds in a row) in our inventory collection (shown in the lesson ) with the $push operator?

 * Yes
 * No

See [answer](./20_double_unwind/ANSWER.md)




## 21. Mapping between SQL and Aggregation
_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_5_Aggregation_Framework/52aa41aae2d4232c54a18b2d/
) - [@Youtube list](http://www.youtube.com/watch?v=auL2R0XKlyM&list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N)_

See [SQL to Aggregation Mapping Chart](http://docs.mongodb.org/manual/reference/sql-aggregation-comparison/)





## 22. Some Common SQL examples
_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_5_Aggregation_Framework/52aa41cce2d4232c54a18b2f/) - [@Youtube list](http://www.youtube.com/watch?v=ep2gLSR6C0U&list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N)_

See [Examples in SQL to Aggregation Mapping Chart](http://docs.mongodb.org/manual/reference/sql-aggregation-comparison/#examples)




## 23.Limitations of the Aggregation Framework
_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_5_Aggregation_Framework/52aa41f1e2d4232c54a18b31/) - [@Youtube list](http://www.youtube.com/watch?v=8BQzKXI-_wE&list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N)_





## [Homework 5.1 (Hands On)](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_5_Aggregation_Framework/52cf2c07e2d423570a05b931/)

**Finding the most frequent author of comments on your blog**

In this assignment you will use the aggregation framework to find the most frequent author of comments on your blog. We will be using the same basic dataset as last week, with posts and comments shortened considerably, and with many fewer documents in the collection in order to streamline the operations of the Hands On web shell.

Use the aggregation framework in the web shell to calculate the author with the greatest number of comments.

To help you verify your work before submitting, the author with the fewest comments is Cody Strouth and he commented 68 times.

Once you've found the correct answer with your query, please choose your answer below for the most prolific comment author.

Note: this data set is relatively large. Due to some quirks of the shell, the entire result set gets pulled into the browser on find(), so if you want to see the document schema, we recommend either using db.posts.findOne(), db.posts.find().limit(1), or that you plan on waiting for a bit after you hit enter. We also recommend that the last phase of your aggregation pipeline is {$limit: 1} (or some single digit number)

Data set is like:
```
> db.posts.findOne()
{
    "_id" : ObjectId("530160c0132c1f4f4005cac1"),
    "body" : "empty_post",
    "permalink" : "cxzdzjkztkqraoqlgcru",
    "author" : "machine",
    "title" : "US Constitution",
    "tags" : [
        "january",
        "mine",
        "modem",
        "literature",
        "saudi arabia",
        "rate",
        "package",
        "respect",
        "bike",
        "cheetah"
    ],
    "comments" : [
        {
            "body" : "empty_comment",
            "email" : "eAYtQPfz@kVZCJnev.com",
            "author" : "Kayce Kenyon"
        },
        {
            "body" : "empty_comment",
            "email" : "HWKaDZPm@atcmDqqF.com",
            "author" : "Devorah Smartt"
        }
    ]
}
```

 * Kayce Kenyon
 * Devorah Smartt
 * Gisela Levin
 * Brittny Warwick
 * Tamika Schildgen
 * Mariette Batdorf


_Answer is: **Gisela Levin**_


```
db.posts.aggregate([{$unwind:"$comments"}, {$group: {_id:"$comments.author", num:{$sum:1}}}, {$sort: {num:-1}}, {$limit:1}])

{
    "ok" : 1,
    "result" : [
        {
            "_id" : "Gisela Levin",
            "num" : 112
        }
    ]
}
```



## [Homework 5.2 (Hands On)](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_5_Aggregation_Framework/52aa426fe2d4232c54a18b37/)

**Crunching the Zipcode dataset**

Please calculate the average population of cities in California (abbreviation CA) and New York (NY) (taken together) with populations over 25,000.

For this problem, assume that a city name that appears in more than one state represents two separate cities.

Please round the answer to a whole number.
Hint: The answer for CT and NJ (using this data set) is 38177.

Please note:
One zip code may cover cities in different states.
Different states might have the same city name.
A city might have multiple zip codes.


Once you've generated your aggregation query, select your answer from the choices below.

For purposes of keeping the Hands On shell quick, we have used a subset of the data you previously used in zips.json, not the full set. This is why there are only 200 documents (and 200 zip codes), and all of them are in New York, Connecticut, New Jersey, and California.

```
> db.zips.findOne()
{
    "_id" : "92278",
    "city" : "TWENTYNINE PALMS",
    "state" : "CA",
    "pop" : 11412,
    "loc" : [
        -116.06041,
        34.237969
    ]
}
```

 * 44805
 * 55921
 * 67935
 * 71819
 * 82426
 * 93777


_Answer is: **44805**_


```
db.zips.aggregate([
    {$match: {state: {$in: ["CA", "NY"]}}},
    {$group: {_id: {state: "$state", city: "$city"}, pop: {$sum: "$pop"}}},
    {$match: {pop: {$gt:25000}}},
    {$group: {_id: null, average: {$avg:"$pop"}}}
])

{
    "ok" : 1,
    "result" : [
        {
            "_id" : null,
            "average" : 44804.782608695656
        }
    ]
}
```




## [Homework 5.3 (Hands On)](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_5_Aggregation_Framework/52aa45b2e2d4232c54a18b3a/)

**Who's the easiest grader on campus?**

A set of grades are loaded into the grades collection.

The documents look like this:

```
{
    "_id" : ObjectId("50b59cd75bed76f46522c392"),
    "student_id" : 10,
    "class_id" : 5,
    "scores" : [
        {
            "type" : "exam",
            "score" : 69.17634380939022
        },
        {
            "type" : "quiz",
            "score" : 61.20182926719762
        },
        {
            "type" : "homework",
            "score" : 73.3293624199466
        },
        {
            "type" : "homework",
            "score" : 15.206314042622903
        },
        {
            "type" : "homework",
            "score" : 36.75297723087603
        },
        {
            "type" : "homework",
            "score" : 64.42913107330241
        }
    ]
}
```

There are documents for each student (student_id) across a variety of classes (class_id). Note that not all students in the same class have the same exact number of assessments. Some students have three homework assignments, etc.

Your task is to calculate the class with the best average student performance. This involves calculating an average for each student in each class of all non-quiz assessments and then averaging those numbers to get a class average. To be clear, each student's average includes only exams and homework grades. Don't include their quiz scores in the calculation.

What is the class_id which has the highest average student perfomance?

Hint/Strategy: You need to group twice to solve this problem. You must figure out the GPA that each student has achieved in a class and then average those numbers to get a class average. After that, you just need to sort. The hardest class is class_id=2. Those students achieved a class average of 37.6

Below, choose the class_id with the highest average student average.

 * 8
 * 9
 * 1
 * 5
 * 7
 * 0
 * 6


_Answer is: **1**_


```
db.grades.aggregate([
    {$unwind: "$scores"},
    {$match: {"scores.type": {$in:["exam", "homework"]}}},
    {$group: {_id: {class_id: "$class_id", student_id: "$student_id"}, gpa: {$avg: "$scores.score"}}},
    {$project: {_id:0, student_id:"$_id.student_id", class_id:"$_id.class_id", gpa:1}},
    {$group: {_id: {class_id: "$class_id"}, average: {$avg: "$gpa"}}},
    {$sort: {average: -1}},
    {$limit: 5}
])

{
    "ok" : 1,
    "result" : [
        {
            "_id" : 1,
            "average" : 64.50642324269175
        },
        {
            "_id" : 5,
            "average" : 58.084487676135495
        },
        {
            "_id" : 20,
            "average" : 57.6309834548989
        },
        {
            "_id" : 26,
            "average" : 56.06918278769095
        },
        {
            "_id" : 9,
            "average" : 55.56861693456624
        }
    ]
}
```




## [Homework 5.4](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_5_Aggregation_Framework/52aa45fee2d4232c54a18b3d/)

**Removing Rural Residents**

In this problem you will calculate the number of people who live in a zip code in the US where the city starts with a digit. We will take that to mean they don't really live in a city. Once again, you will be using the zip code collection, which you will find in the 'handouts' link in this page. Import it into your mongod using the following command from the command line:

```
> mongoimport -d test -c zips --drop zips.json
```

If you imported it correctly, you can go to the test database in the mongo shell and conform that

```
> db.zips.count()
```

yields 29,467 documents.

The project operator can extract the first digit from any field. For example, to extract the first digit from the city field, you could write this query:

```
db.zips.aggregate([
    {$project:
     {
        first_char: {$substr : ["$city",0,1]},
     }
   }
])
```

Using the aggregation framework, calculate the sum total of people who are living in a zip code where the city starts with a digit. Choose the answer below.

_Note that you will need to probably change your projection to send more info through than just that first character. Also, you will need a filtering step to get rid of all documents where the city does not start with a digital (0-9)._

 * 298015
 * 345232
 * 245987
 * 312893
 * 158249
 * 543282


_Answer is: **298015**_


```
db.zips.aggregate([
    {$project: { first_char: {$substr : ["$city",0,1]}, pop: 1 } },
    {$match: {first_char: {$in: ['0','1','2','3','4','5','6','7','8','9']}}},
    {$group: {_id: null, total_population: {$sum: "$pop"}}}
])

{
    "result" : [
        {
            "_id" : null,
            "total_population" : 298015
        }
    ],
    "ok" : 1
}
```

