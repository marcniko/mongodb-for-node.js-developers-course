# [<< Week 5](./README.md) < 10. Using push - Answer
_[@Youtube video answer](http://www.youtube.com/watch?v=hFqDvVXtm6E&index=21&list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N)_

**Would you expect the following two queries to produce the same result or different results?**

```
db.zips.aggregate([{"$group":{"_id":"$city", "postal_codes":{"$push":"$_id"}}}])
```
```
db.zips.aggregate([{"$group":{"_id":"$city", "postal_codes":{"$addToSet":"$_id"}}}])
```

 * **Same result**
 * Different Result
 
_Answer: same result_
_Because **_id** is always unique._