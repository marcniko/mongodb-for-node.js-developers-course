# [<< Week 5](../README.md) < 12. Double Group Stage - Answer
_[@Youtube video answer](http://www.youtube.com/watch?v=J-asAAEHJ0Q&index=25&list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N)_

## Quiz:
Given the following collection:

```
> db.fun.find()
{ "_id" : 0, "a" : 0, "b" : 0, "c" : 21 }
{ "_id" : 1, "a" : 0, "b" : 0, "c" : 54 }
{ "_id" : 2, "a" : 0, "b" : 1, "c" : 52 }
{ "_id" : 3, "a" : 0, "b" : 1, "c" : 17 }
{ "_id" : 4, "a" : 1, "b" : 0, "c" : 22 }
{ "_id" : 5, "a" : 1, "b" : 0, "c" : 5 }
{ "_id" : 6, "a" : 1, "b" : 1, "c" : 87 }
{ "_id" : 7, "a" : 1, "b" : 1, "c" : 97 }
```

And the following aggregation query
```
db.fun.aggregate([{$group:{_id:{a:"$a", b:"$b"}, c:{$max:"$c"}}}, {$group:{_id:"$_id.a", c:{$min:"$c"}}}])
```

**What values are returned?**

 * 17 and 54
 * 97 and 21
 * 54 and 5
 * **52 and 22**

_Answer is: 52 and 22_

 1. First group return maxs c by grouping a and b:
```
{"result":
  [
    { "_id" : {"a" : 0, "b" : 0}, "c" : 54 },
    { "_id" : {"a" : 0, "b" : 1}, "c" : 52 },
    { "_id" : {"a" : 1, "b" : 0}, "c" : 22 },
    { "_id" : {"a" : 1, "b" : 1}, "c" : 97 }
  ]
}
```
 2. Second group return mins c by grouping a:
```
{"result":
  [
    { "_id" : "a" : 0, "c" : 52 },
    { "_id" : "a" : 1, "c" : 22 }
  ]
}
```
