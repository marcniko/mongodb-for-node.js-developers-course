# [<< Week 5](../README.md) < 16. $limit and $skip - Answer
_[@Youtube video answer](http://www.youtube.com/watch?v=joRw-fqCIWA&list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N)_

**How many documents do you think will be in the result set?**

 * 10
 * 5
 * **0**
 * 100

_Answer is: 0_

Check the results quering it, see [quiz_using_limit_and_skip.js](./quiz_using_limit_and_skip.js)
```
mongo < 16_limit_and_skip/quiz_using_limit_and_skip.js
```
