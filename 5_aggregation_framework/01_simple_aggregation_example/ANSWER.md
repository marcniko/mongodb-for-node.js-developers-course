# [<< Week 5](../README.md) < [1. Simple Agregation Example](./README.md) - Answer
_[@Youtube video answer](https://www.youtube.com/watch?v=Nt0cQI86G40&list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N)_

**Write the aggregation query that will find the number of products by category of a collection that has the form:**

```
{
	"_id" : ObjectId("50b1aa983b3d0043b51b2c52"),
	"name" : "Nexus 7",
	"category" : "Tablets",
	"manufacturer" : "Google",
	"price" : 199
}
```

_Answer is:_
```
db.products.aggregate([
    {$group:
        {
            _id: "$category",
            num_products: {$sum:1}
        }
    }
])
```
