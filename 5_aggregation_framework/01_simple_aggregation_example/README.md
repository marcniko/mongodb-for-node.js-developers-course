# [<< Week 5](../README.md) < 1. Simple Agregation Example
_[Video lecture](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_5_Aggregation_Framework/52aa3411e2d4232c54a18ada/) - [@Youtube list](https://www.youtube.com/watch?v=T2BX-LZOYsY&list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N)_

## Example:
 * Import products
```
mongo < products.js
```

 * execute simple aggregate example
```
mongo < simple_example.js

MongoDB shell version: 2.4.9
connecting to: test
switched to db agg
{
    "result" : [
        {
            "_id" : "Amazon",
            "num_products" : 2
        },
        {
            "_id" : "Sony",
            "num_products" : 1
        },
        {
            "_id" : "Samsung",
            "num_products" : 2
        },
        {
            "_id" : "Google",
            "num_products" : 1
        },
        {
            "_id" : "Apple",
            "num_products" : 4
        }
    ],
    "ok" : 1
}
bye
```

## Quiz:

**Write the aggregation query that will find the number of products by category of a collection that has the form:**

```
{
    "_id" : ObjectId("50b1aa983b3d0043b51b2c52"),
    "name" : "Nexus 7",
    "category" : "Tablets",
    "manufacturer" : "Google",
    "price" : 199
}
```

Have the resulting key be called "num_products," as in the video lesson. Hint, you just need to change which key you are aggregating on relative to the examples shown in the lesson.
Please double quote all keys to make it easier to check your result.

See [answer](./ANSWER.md)

