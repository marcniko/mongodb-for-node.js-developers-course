# [<< Week 5](../README.md) < 11. Using max and min - Answer
_[@Youtube video answer](http://www.youtube.com/watch?v=sHCdOiCispA&index=23&list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N)_

**Again thinking about the zip code database, write an aggregation query that will return the population of the postal code in each state with the highest population.**

_Answer:_
```
db.zips.aggregate([
    {$group:
        {
            _id: "$state",
            "pop": {$max: "$pop"}
        }
    }
])
```

See [quiz_using_max.js](./quiz_using_max.js)
```
mongo < 11_using_max/quiz_using_max.js
```