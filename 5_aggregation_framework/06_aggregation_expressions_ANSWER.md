# [<< Week 5](./README.md) < 6. Aggregation Expressions - Answer
_[@Youtube video answer](https://www.youtube.com/watch?v=kJ0k2na4ukU&list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N)_

**Which of the following aggregation expressions must be used in conjunction with a sort to make any sense?**

 * $addToSet
 * **$first**
 * **$last**
 * $max
 * $min
 * $avg
 * $push
 * $sum

_Answers are: $first and $last_
