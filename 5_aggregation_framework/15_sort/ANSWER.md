# [<< Week 5](../README.md) < 15. $sort - Answer
_[@Youtube video answer](http://www.youtube.com/watch?v=-cghyd6AHHA&list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N)_

_Answer is:_
```
db.zips.aggregate([ {$sort:{state:1, city:1}} ])
```

See [quiz_using_sort.js](./quiz_using_sort.js)
```
mongo < 15_sort/quiz_using_sort.js
```
