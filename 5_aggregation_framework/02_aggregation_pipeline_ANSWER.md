# [<< Week 5](./README.md) < 2. The Aggregation Pipeline - Answer
_[@Youtube video answer](https://www.youtube.com/watch?v=mcnGfI69rRc&list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N)_

**Which of the following are stages in the aggregation pipeline. Check all that apply.**

 1. **Match**
 1. Transpose
 1. **Group**
 1. **Skip**
 1. **Limit**
 1. **Sort**
 1. **Project**
 1. **Unwind**

_Answers are: all except "Transpose"_
