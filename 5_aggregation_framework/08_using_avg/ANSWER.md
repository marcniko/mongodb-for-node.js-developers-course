# [<< Week 5](../README.md) < 7. Using $sum - Answer
_[@Youtube video answer](https://www.youtube.com/watch?feature=player_embedded&v=ATO_s_Ah08o)_

**Write an aggregation expression to calculate the average population of a zip code (postal code) by state. As before, the postal code is in the _id field and is unique. The collection is assumed to be called "zips" and you should name the key in the result set "average_pop".**

_Answer is:_
```
db.zips.aggregate([
	{$group:
		{
		 _id: "$state",
		 average_pop: {$avg:"$pop"}
		}
	}
])
```
or the same inline:
```
db.zips.aggregate([{"$group":{"_id":"$state", "average_pop":{"$avg":"$pop"}}}])
```
