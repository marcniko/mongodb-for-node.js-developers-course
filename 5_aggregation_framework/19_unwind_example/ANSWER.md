# [<< Week 5](../README.md) < 19. $unwind example - Answer
_[@Youtube video answer](http://www.youtube.com/watch?v=jAWL-BJD0tI&list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N)_

**Which grouping operator will enable to you to reverse the effects of an unwind?**

 * $sum
 * $addToSet
 * **$push**
 * $first

_Answer is: $push_