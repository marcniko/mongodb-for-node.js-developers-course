# [<< Week 5](./README.md) < 3. Simple Example Expanded - Answer
_[@Youtube video answer](https://www.youtube.com/watch?v=nr1E1qTyIHU&list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N)_

**How many documents will be in the result set from aggregate?**

 * 1
 * 2
 * **3**
 * 4
 * 5

_Answer is: 3 _
