# [Node.js Driver: find, findOne, and cursors](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_2_CRUD/52d04ae9e2d423570a05b997/)
_https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_2_CRUD/52d04ae9e2d423570a05b997/_

## Import grades.json to grades collection in course database
```
mongoimport -d course -c grades grades.json
```

## Installation mongodb package for node.js
```
npm install mongodb
```

## Run app.js
```
node app.js
```
