# [Node.js Driver: Using Field Projection](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_2_CRUD/52d04b4de2d423570a05b99b/)
_https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_2_CRUD/52d04b4de2d423570a05b99b/_

## Installation mongodb package for node.js
```
npm install mongodb
```

## Run app.js
```
node app.js
```

## Quiz _(quiz.txt)_:
Which of the following queries will cause only the "grade" field to be returned?

 1. db.collection('grades').find({ 'grades' : 0, '_id' : 1 }, callback);
 1. db.collection('grades').find({ 'grades' : 1, '_id' : 0 }, callback);
 1. db.collection('grades').find({}, { 'grades' : 1, '_id' : 0 }, callback);
 1. db.collection('grades').find({}, { 'grades' : 1 }, callback);

_Answer: 3_
