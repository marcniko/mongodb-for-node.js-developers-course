# [<<](./README.md) Answer for [Homework: Homework 4.4](./README.md)

## What is the latency of the longest running operation to the collection, in milliseconds?

```
> db.profile.find({ns:"school2.students"}, {millis:1}).sort({millis:-1}).limit(1).pretty()
{ "_id" : ObjectId("52f8e6317dcf6b479eb971a2"), "millis" : 15820 }
```

 1. 4715

 1. 34430

 1. 5018

 1. **15820**

 1. 3217


_Answer is: 15820_
