# Homework: Homework 4.4
_[https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_4_Performance/52cf29bae2d423570a05b92d/](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_4_Performance/52cf29bae2d423570a05b92d/)_

In this problem you will analyze a profile log taken from a mongoDB instance. To start, please download sysprofile.json from Download Handout link and import it with the following command:

```
mongoimport -d m101 -c profile < sysprofile.json
```

Now query the profile data, looking for all queries to the students collection in the database school2, sorted in order of decreasing latency.


**What is the latency of the longest running operation to the collection, in milliseconds?**

 1. 4715

 1. 34430

 1. 5018

 1. 15820

 1. 3217

See [ANSWER.md](./ANSWER.md)
