# [<<](./README.md) Answer for [Homework: Homework 4.3](./README.md)

## Queries & explains

```
db.posts.find().sort({ date : -1}).limit(10).explain()

db.posts.find({tags : 'crab'}).sort({ date : -1}).limit(10).explain()

db.posts.find({permalink: 'mxwnnnqaflufnqwlekfd'}).limit(1).explain()
```

## Create indexes

```
db.posts.ensureIndex({ date : -1 })

db.posts.ensureIndex({ tags : 1 })

db.posts.ensureIndex({ permalink : 1 }, { unique : true })
```


## Validate

```
$ node hw4-3_validate.js

Welcome to the HW 4.3 Checker. My job is to make sure you added the indexes
that make the blog fast in the following three situations
	When showing the home page
	When fetching a particular post
	When showing all posts for a particular tag
Data looks like it is properly loaded into the posts collection
Home page is super fast. Nice job.

Blog retrieval by permalink is super fast. Nice job.

Blog retrieval by tag is super fast. Nice job.

hw4-3 Validated successfully!
Your validation code is: dOiP6E1JDOYwDKWP0uN3
```

_Answer: dOiP6E1JDOYwDKWP0uN3_
