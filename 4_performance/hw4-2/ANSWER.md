# [<<](./README.md) Answer for [Homework: Homework 4.2](./README.md)

## What can you infer from the explain output?

 1. **This query performs a collection scan.**

 1. **The query uses an index to determine the order in which to return result documents.**

 1. The query uses an index to determine which documents match.

 1. The query returns 46462 documents.

 1. **The query visits 46462 documents.**

 1. The query is a "covered index query".

_Answers are: 1, 2 and 5_
