# [<<](./README.md) Answer for [Homework: Homework 4.1](./README.md)

## Which of the following queries can utilize an index. Check all that apply.

 1. db.products.find({'brand':"GE"})

 1. **db.products.find({'brand':"GE"}).sort({price:1})**

 1. **db.products.find({$and:[{price:{$gt:30}},{price:{$lt:50}}]}).sort({brand:1})**

 1. db.products.find({brand:'GE'}).sort({category:1, brand:-1}).explain()

_Answers are: 2 and 3_
