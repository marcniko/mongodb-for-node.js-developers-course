# Homework: Homework 4.1
_[https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_4_Performance/52aa2f48e2d4232c54a18ac9/](https://education.mongodb.com/courses/10gen/M101JS/2014_January/courseware/Week_4_Performance/52aa2f48e2d4232c54a18ac9/)_

Suppose you have a collection with the following indexes:

```
> db.products.getIndexes()
[
	{
		"v" : 1,
		"key" : {
			"_id" : 1
		},
		"ns" : "store.products",
		"name" : "_id_"
	},
	{
		"v" : 1,
		"key" : {
			"sku" : 1
		},
                "unique" : true,
		"ns" : "store.products",
		"name" : "sku_1"
	},
	{
		"v" : 1,
		"key" : {
			"price" : -1
		},
		"ns" : "store.products",
		"name" : "price_-1"
	},
	{
		"v" : 1,
		"key" : {
			"description" : 1
		},
		"ns" : "store.products",
		"name" : "description_1"
	},
	{
		"v" : 1,
		"key" : {
			"category" : 1,
			"brand" : 1
		},
		"ns" : "store.products",
		"name" : "category_1_brand_1"
	},
	{
		"v" : 1,
		"key" : {
			"reviews.author" : 1
		},
		"ns" : "store.products",
		"name" : "reviews.author_1"
	}
```

**Which of the following queries can utilize an index. Check all that apply.**

 1. db.products.find({'brand':"GE"})

 1. db.products.find({'brand':"GE"}).sort({price:1})

 1. db.products.find({$and:[{price:{$gt:30}},{price:{$lt:50}}]}).sort({brand:1})

 1. db.products.find({brand:'GE'}).sort({category:1, brand:-1}).explain()

See [ANSWER.md](./ANSWER.md)
