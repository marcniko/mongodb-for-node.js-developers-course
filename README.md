# M101JS: MongoDB for Node.js Developers (2014 January)
_[Course @MongoDB University](https://education.mongodb.com/courses/10gen/M101JS/2014_January/info)_

[Course Syllabus (pdf)](./M101JS - Syllabus.pdf)

## Week 1 Introduction
_[Lectures and quizes](./1_introduction/)_

_[List videos @Youtube]()_

## Week 2 CRUD
_[Lectures and quizes](./2_CRUD/)_

_[List videos @Youtube](https://www.youtube.com/playlist?list=PLUVUdDGxXszhzCLg8n0BQZPz0O8vF-ZlN)_

## Week 3 Schema Design
_[Lectures and quizes](./3_schema_design/)_

_[List videos @Youtube]()_

## Week 4 Performance
_[Lectures and quizes](./4_performance/)_

_[List videos @Youtube]()_

## Week 5 Aggregation Framework
_[Lectures and quizes](./5_aggregation_framework/README.md)_

_[List videos @Youtube](https://www.youtube.com/playlist?list=PLUVUdDGxXszi2kCi8iSrbIjLh1aevBQ1N)_

## Week 6 Appplication Engineering
_[Lectures and quizes](./6_application_engineering/README.md)_

_[List videos @Youtube](https://www.youtube.com/playlist?list=PLUVUdDGxXszgq6abyzMsbQ4omDp6UL2J1)_

## Week 7 Mongoose
_[List videos @Youtube](https://www.youtube.com/playlist?list=PLUVUdDGxXszhdfS-EjQVxdZIouEo4Yjk_)_

_[Lesson 7 examples](./7_mongoose/lesson7_examples/) ([lesson7_examples.zip](./7_mongoose/lesson7_examples.zip))_

## Final Exam
_[Questions](./8_final_exam/README.md)_